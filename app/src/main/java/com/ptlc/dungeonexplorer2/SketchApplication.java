package com.ptlc.dungeonexplorer2;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Process;
import java.io.PrintWriter;
import java.io.StringWriter;

public class SketchApplication extends Application {
    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler;

    static String getStackTrace(Throwable paramThrowable) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        while (true) {
            String str;
            if (paramThrowable == null) {
                str = stringWriter.toString();
                printWriter.close();
                return str;
            }
            paramThrowable.printStackTrace(printWriter);
            Throwable throwable = paramThrowable.getCause();
        }
    }

    public void onCreate() {
        this.uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread param1Thread, Throwable param1Throwable) {
                Intent intent = new Intent(SketchApplication.this.getApplicationContext(), DebugActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("error", SketchApplication.this.getStackTrace(param1Throwable));
                PendingIntent pendingIntent = PendingIntent.getActivity(SketchApplication.this.getApplicationContext(), 11111, intent, PendingIntent.FLAG_ONE_SHOT);
                ((AlarmManager)SketchApplication.this.getSystemService(ALARM_SERVICE)).set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000L, pendingIntent);
                Process.killProcess(Process.myPid());
                System.exit(2);
                SketchApplication.this.uncaughtExceptionHandler.uncaughtException(param1Thread, param1Throwable);
                /*AlertDialog.Builder exitDialog = new AlertDialog.Builder(SketchApplication.this.getApplicationContext());
                exitDialog.setTitle("Erreur");
                exitDialog.setMessage(SketchApplication.this.getStackTrace(param1Throwable));
                exitDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface _dialog, int _which) {
                    }
                });
                exitDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface _dialog, int _which) {
                    }
                });
                exitDialog.create().show();*/
                //System.exit(2);
            }
        });
        super.onCreate();
    }
}
