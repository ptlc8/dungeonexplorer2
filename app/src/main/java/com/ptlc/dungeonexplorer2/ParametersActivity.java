package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class ParametersActivity extends Activity {

    private MediaPlayer SoundTest;
    private EditText default_arme;
    private Switch mojis_blocker;
    private SharedPreferences parametres;
    private TextView sound_test;
    private TextView text_coup_de;
    private TextView text_default_arme;
    private TextView text_mojis_blocker;
    private TextView text_volume;
    private Switch volume;

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.parameters);
        initialize();
    }

    private void initialize() {
        this.text_mojis_blocker = findViewById(R.id.text_mojis_blocker);
        this.mojis_blocker = findViewById(R.id.mojis_blocker);
        this.text_volume = findViewById(R.id.text_volume);
        this.sound_test = findViewById(R.id.sound_test);
        this.volume = findViewById(R.id.volume);
        this.text_default_arme = findViewById(R.id.text_default_arme);
        this.text_coup_de = findViewById(R.id.text_coup_de);
        this.default_arme = findViewById(R.id.default_arme);
        this.parametres = getSharedPreferences("parametres.saucisse", 0);
        this.default_arme.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable param1Editable) {}
            public void beforeTextChanged(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {}
            public void onTextChanged(CharSequence charSequence, int param1Int1, int param1Int2, int param1Int3) {
                ParametersActivity.this.parametres.edit().putString("default_arme", charSequence.toString()).commit();
            }
        });
        this.sound_test.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                if (ParametersActivity.this.volume.isChecked()) {
                    ParametersActivity.this.SoundTest = MediaPlayer.create(ParametersActivity.this.getApplicationContext(), R.raw.gameboy_startup_sound);
                    ParametersActivity.this.SoundTest.start();
                }
            }
        });
        this.volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                ParametersActivity.this.parametres.edit().putString("volume", checked ? String.valueOf(1L) : String.valueOf(0L)).commit();
            }
        });
        this.mojis_blocker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                ParametersActivity.this.parametres.edit().putString("mojis", checked ? String.valueOf(0L) : String.valueOf(1L)).commit();
            }
        });

        this.default_arme.setText(this.parametres.getString("default_arme", ""));
        this.volume.setChecked(Double.parseDouble(this.parametres.getString("volume", "1")) == 1.0D);
        if (this.parametres.getString("mojis", "").equals("0"))
            this.text_default_arme.setText(this.text_default_arme.getText().toString().replace("?", "?"));
    }

    private void showMessage(String paramString) { Toast.makeText(getApplicationContext(), paramString, Toast.LENGTH_SHORT).show(); }

}
