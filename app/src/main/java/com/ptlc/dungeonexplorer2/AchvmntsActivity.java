package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class AchvmntsActivity extends Activity {

    private SharedPreferences achevements;

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.achvmnts);
        initialize();
    }

    private void initialize() {
        TextView games_10 = findViewById(R.id.games_10);
        TextView games_50 = findViewById(R.id.games_50);
        TextView games_100 = findViewById(R.id.games_100);
        TextView pire_mort = findViewById(R.id.pire_mort);
        TextView vieux_fou = findViewById(R.id.vieux_fou);
        TextView collectator_pierres = findViewById(R.id.collectator_pierres);
        TextView collectator_pierres_d = findViewById(R.id.collectator_pierres_);
        TextView revendeur_pierres = findViewById(R.id.revendeur_pierres);
        TextView batons_magiques = findViewById(R.id.batons_magiques);
        TextView batons_magiques_d = findViewById(R.id.batons_magiques_d);
        TextView vieux_fou_2 = findViewById(R.id.vieux_fou_2);
        TextView vieux_fou_2_d = findViewById(R.id.vieux_fou_2_d);
        TextView illogisme = findViewById(R.id.illogisme);
        this.achevements = getSharedPreferences("succ�s.pkm", 0);

        batons_magiques_d.setText(batons_magiques_d.getText().toString().replace("�", this.achevements.getString("batons_magiques", "")));
        collectator_pierres_d.setText(collectator_pierres_d.getText().toString().replace("�", this.achevements.getString("pierres", "")));


        if (this.achevements.getString("illogisme", "").equals("1"))
            illogisme.setText(illogisme.getText().toString().replace("?", "?"));
        if (this.achevements.getString("revendeur_pierres", "").equals("1"))
            revendeur_pierres.setText(revendeur_pierres.getText().toString().replace("?", "?"));
        if (this.achevements.getString("batons_magiques", "").equals("3"))
            batons_magiques.setText(batons_magiques.getText().toString().replace("?", "?"));
        if (this.achevements.getString("pire_mort", "").equals("1"))
            pire_mort.setText(pire_mort.getText().toString().replace("?", "?"));
        if (this.achevements.getString("pierres", "").equals("4"))
            collectator_pierres.setText(collectator_pierres.getText().toString().replace("?", "?"));
        if (this.achevements.getString("vieux_fou", "").equals("1")) {
            vieux_fou.setText(vieux_fou.getText().toString().replace("?", "?"));
            vieux_fou_2.setText("Le retour du vieux fou");
            vieux_fou_2_d.setText("croiser 2 fois le vieux fou en une seule aventure");
        }
        if (this.achevements.getString("vieux_fou_2", "").equals("1"))
            vieux_fou_2.setText(vieux_fou_2.getText().toString().replace("?", "?"));
        if (this.achevements.getString("games", "").equals("10"))
            games_10.setText(games_10.getText().toString().replace("?", "?"));
        if (this.achevements.getString("games", "").equals("50")) {
            games_50.setText(games_50.getText().toString().replace("?", "?"));
        }
        if (this.achevements.getString("games", "").equals("100"))
            games_100.setText(games_100.getText().toString().replace("?", "?"));
    }

}
