package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class InternetActivity extends Activity {
    private WebView webview1;

    private int getRandom(int paramInt1, int paramInt2) { return (new Random()).nextInt(paramInt2 - paramInt1 + 1) + paramInt1; }

    private void initialize() {
        this.webview1 = (WebView)findViewById(R.id.webview1);
        this.webview1.getSettings().setJavaScriptEnabled(true);
        this.webview1.getSettings().setSupportZoom(true);
        this.webview1.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView param1WebView, String param1String) { super.onPageFinished(param1WebView, param1String); }

            public void onPageStarted(WebView param1WebView, String param1String, Bitmap param1Bitmap) {
                if (!param1String.contains("ptlcft") && !param1String.contains("ptft"))
                    InternetActivity.this.webview1.loadUrl(InternetActivity.this.getIntent().getStringExtra("url"));
                super.onPageStarted(param1WebView, param1String, param1Bitmap);
            }
        });
    }

    private void initializeLogic() { this.webview1.loadUrl(getIntent().getStringExtra("url")); }

    private void showMessage(String paramString) { Toast.makeText(getApplicationContext(), paramString, 0).show(); }

    public ArrayList<Double> getCheckedItemPositionsToArray(ListView paramListView) {
        ArrayList arrayList = new ArrayList();
        SparseBooleanArray sparseBooleanArray = paramListView.getCheckedItemPositions();
        for (byte b = 0;; b++) {
            if (b >= sparseBooleanArray.size())
                return arrayList;
            if (sparseBooleanArray.valueAt(b))
                arrayList.add(Double.valueOf(sparseBooleanArray.keyAt(b)));
        }
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.internet);
        initialize();
        initializeLogic();
    }
}
