package com.ptlc.dungeonexplorer2;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exchange {

    private Item result;
    private int resultAmount;
    private List<Pair<Item, Integer>> items;

    public Exchange(Item result, int amount, Pair<Item, Integer>... items) {
        this.result = result;
        this.resultAmount = amount;
        this.items = Arrays.asList(items);
    }

    @Override @NonNull
    public String toString() {
        String s = "";
        for (int i = 0; i < items.size(); i++) {
            s += items.get(i).first.getName() + " (×" + items.get(i).second + ")";
            if (i < items.size()-1) s += " + ";
        }
        s += " → " + result.getName() + "(×" + resultAmount + ")";
        return s;
    }

    public boolean make(Player player) {
        for (Pair<Item, Integer> i : items) {
            if (!player.has(i.first, i.second)) return false;
        }
        for (Pair<Item, Integer> i : items) {
            player.remove(i.first, i.second);
        }
        player.give(result, resultAmount);
        return true;
    }

    public Item getResult() {
        return result;
    }

    public int getResultAmount() {
        return resultAmount;
    }

    public int getItemsNumber() {
        return items.size();
    }

    public Item getItem(int i) {
        return items.get(i).first;
    }

    public int getAmount(int i) {
        return items.get(i).second;
    }

}
