package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private AlertDialog.Builder ExitDialog;

    private SharedPreferences HighScore;
    private SharedPreferences achievements;
    private SharedPreferences para_saucisses;
    private SharedPreferences stats;

    private Intent intent = new Intent();

    private TextView achievementsButton;
    private TextView parametresButton;
    private TextView informationsButton;
    private TextView ptlcftButton;
    
    private TextView bestscore;
    private TextView bestargent;

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.main);
        initialize();
    }

    @Override
    public void onStart() {
        super.onStart();
        this.bestscore.setText(this.HighScore.getString("1s", ""));
        this.bestargent.setText(this.HighScore.getString("1a", ""));
        //this.HighScore.getString("pseudo", "").equals("");
        if (this.para_saucisses.getString("mojis", "").equals("1")) {
            this.achievementsButton.setText("?");
            this.parametresButton.setText("?");
            this.informationsButton.setText("?");
            this.ptlcftButton.setText("?");
        }
        if (this.para_saucisses.getString("mojis", "").equals("0")) {
            this.achievementsButton.setText("?");
            this.parametresButton.setText("?");
            this.informationsButton.setText("?�");
            this.ptlcftButton.setText("?");
        }
    }

    private void initialize() {
        Button startbutton = findViewById(R.id.startbutton);
        TextView version = findViewById(R.id.version);
        Button extraordinary_shop = findViewById(R.id.extraordinary_shop);
        this.bestscore = findViewById(R.id.bestscore);
        this.bestargent = findViewById(R.id.bestargent);
        this.achievementsButton = findViewById(R.id.achievements);
        this.parametresButton = findViewById(R.id.parametres);
        this.informationsButton = findViewById(R.id.informations);
        this.ptlcftButton = findViewById(R.id.ptlcft);
        this.HighScore = getSharedPreferences("LaCr�meDeLaCr�me", 0);
        this.ExitDialog = new AlertDialog.Builder(this);
        this.para_saucisses = getSharedPreferences("parametresButton.saucisse", 0);
        this.stats = getSharedPreferences("stats.txt", 0);
        this.achievements = getSharedPreferences("succ�s.pkm", 0);
        startbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                MainActivity.this.intent.setClass(MainActivity.this.getApplicationContext(),
                        Game2Activity.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });
        this.ptlcftButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                MainActivity.this.intent.putExtra("url", "https://ptlcft.wordpress.com");
                MainActivity.this.intent.setClass(MainActivity.this.getApplicationContext(), InternetActivity.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });
        this.informationsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                MainActivity.this.intent.putExtra("url", "https://ptlcft.wordpress.com/contact");
                MainActivity.this.intent.setClass(MainActivity.this.getApplicationContext(),
                        InternetActivity.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });
        this.parametresButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                MainActivity.this.intent.setClass(MainActivity.this.getApplicationContext(), ParametersActivity.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });
        this.achievementsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                MainActivity.this.intent.setClass(MainActivity.this.getApplicationContext(), AchvmntsActivity.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });
        extraordinary_shop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                MainActivity.this.intent.setClass(MainActivity.this.getApplicationContext(), ExtraShopActivity.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });

        version.setText(R.string.version);
        this.bestscore.setText(this.HighScore.getString("1s", ""));
        this.bestargent.setText(this.HighScore.getString("1a", ""));
        //this.HighScore.getString("pseudo", "").equals("");
        if (!this.HighScore.getString("first", "").equals("q")) {
            this.HighScore.edit().putString("first", "q").apply();
            this.HighScore.edit().putString("TotalCoins", "0").apply();
            this.para_saucisses.edit().putString("default_arme", "poing").apply();
            this.para_saucisses.edit().putString("volume", "1").apply();
        }
        if (!this.HighScore.getString("second", "").equals("j")) {
            this.HighScore.edit().putString("second", "j").apply();
            this.para_saucisses.edit().putString("mojis", "1").apply();
            this.stats.edit().putString("base_arme", "rien").apply();
            this.stats.edit().putString("sup_points", "0").apply();
        }
        if (!this.HighScore.getString("second2", "").equals("f")) {
            this.HighScore.edit().putString("second2", "f").apply();
            this.achievements.edit().putString("games", "0").apply();
        }
    }

    private void showMessage(String paramString) { Toast.makeText(getApplicationContext(), paramString, Toast.LENGTH_SHORT).show(); }

    @Override
    public void onBackPressed() {
        this.ExitDialog.setTitle("? Quitter ?");
        this.ExitDialog.setMessage("Voulez-vous vraiment quitter cette merveilleuse application d'exploration de donjon ? ?");
        this.ExitDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface param1DialogInterface, int param1Int) {
                MainActivity.this.showMessage("Donne ton avis ? sur ptlcft.wordpress.com/contact");
                MainActivity.this.finish();
            }
        });
        this.ExitDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface param1DialogInterface, int param1Int) { MainActivity.this.showMessage("?"); }
        });
        this.ExitDialog.create().show();
    }

}
