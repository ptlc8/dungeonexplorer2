package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class SetStatsActivity extends Activity {
    private TextView agilite;

    private Button agilite_moins;

    private Button agilite_plus;

    private TextView charisme;

    private Button charisme_moins;

    private Button charisme_plus;

    private TextView force;

    private Button force_moins;

    private Button force_plus;

    private HorizontalScrollView hscroll1;

    private LinearLayout linear1;

    private LinearLayout linear2;

    private LinearLayout linear3;

    private LinearLayout linear4;

    private LinearLayout linear5;

    private LinearLayout linear6;

    private SharedPreferences parametres;

    private SharedPreferences set_stats;

    private TextView sexe;

    private TextView title;

    private TextView title_agilite;

    private TextView title_charisme;

    private TextView title_force;

    private TextView title_sexe;

    private TextView title_vie;

    private TextView vie;

    private ScrollView vscroll1;

    private void _actualiser() {
        if (this.parametres.getString("mojis", "").equals("1")) {
            this.vie.setText(this.set_stats.getString("Vie", "").concat(" ❤"));
            this.force.setText(this.set_stats.getString("Force", "").concat(" \uD83C\uDFCB"));
            this.agilite.setText(this.set_stats.getString("Agilite", "").concat(" \uD83C\uDFC3"));
            this.charisme.setText(this.set_stats.getString("Charisme", "").concat(" \uD83D\uDD76"));
        } else {
            this.vie.setText(this.set_stats.getString("Vie", "").concat(" ♥"));
            this.force.setText(this.set_stats.getString("Force", "").concat(" F"));
            this.agilite.setText(this.set_stats.getString("Agilite", "").concat(" A"));
            this.charisme.setText(this.set_stats.getString("Charisme", "").concat(" C"));
        }
        if (this.set_stats.getString("Sexe", "").equals("1")) {
            this.sexe.setText("?");
            return;
        }
        this.sexe.setText("?");
    }

    private void _modify_stats(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4) {
        this.set_stats.edit().putString("Vie", String.valueOf((long)(Double.parseDouble(this.set_stats.getString("Vie", "")) + paramDouble1))).commit();
        this.set_stats.edit().putString("Force", String.valueOf((long)(Double.parseDouble(this.set_stats.getString("Force", "")) + paramDouble2))).commit();
        this.set_stats.edit().putString("Agilite", String.valueOf((long)(Double.parseDouble(this.set_stats.getString("Agilite", "")) + paramDouble3))).commit();
        this.set_stats.edit().putString("Charisme", String.valueOf((long)(Double.parseDouble(this.set_stats.getString("Charisme", "")) + paramDouble4))).commit();
        if (Double.parseDouble(this.set_stats.getString("Vie", "")) < 2.0D) {
            this.force_plus.setEnabled(false);
            this.agilite_plus.setEnabled(false);
            this.charisme_plus.setEnabled(false);
        } else {
            if (Double.parseDouble(this.set_stats.getString("Force", "")) > 9.0D) {
                this.force_plus.setEnabled(false);
            } else {
                this.force_plus.setEnabled(true);
            }
            if (Double.parseDouble(this.set_stats.getString("Agilite", "")) > 9.0D) {
                this.agilite_plus.setEnabled(false);
            } else {
                this.agilite_plus.setEnabled(true);
            }
            if (Double.parseDouble(this.set_stats.getString("Charisme", "")) > 9.0D) {
                this.charisme_plus.setEnabled(false);
            } else {
                this.charisme_plus.setEnabled(true);
            }
        }
        if (Double.parseDouble(this.set_stats.getString("Force", "")) < 2.0D) {
            this.force_moins.setEnabled(false);
        } else {
            this.force_moins.setEnabled(true);
        }
        if (Double.parseDouble(this.set_stats.getString("Agilite", "")) < 2.0D) {
            this.agilite_moins.setEnabled(false);
        } else {
            this.agilite_moins.setEnabled(true);
        }
        if (Double.parseDouble(this.set_stats.getString("Charisme", "")) < 2.0D) {
            this.charisme_moins.setEnabled(false);
        } else {
            this.charisme_moins.setEnabled(true);
        }
        _actualiser();
    }

    private int getRandom(int paramInt1, int paramInt2) { return (new Random()).nextInt(paramInt2 - paramInt1 + 1) + paramInt1; }

    private void initialize() {
        this.title = (TextView)findViewById(R.id.title);
        this.hscroll1 = (HorizontalScrollView)findViewById(R.id.hscroll1);
        this.vscroll1 = (ScrollView)findViewById(R.id.vscroll1);
        this.linear1 = (LinearLayout)findViewById(R.id.linear1);
        this.linear2 = (LinearLayout)findViewById(R.id.linear2);
        this.linear3 = (LinearLayout)findViewById(R.id.linear3);
        this.linear4 = (LinearLayout)findViewById(R.id.linear4);
        this.linear5 = (LinearLayout)findViewById(R.id.linear5);
        this.linear6 = (LinearLayout)findViewById(R.id.linear6);
        this.title_vie = (TextView)findViewById(R.id.title_vie);
        this.vie = (TextView)findViewById(R.id.vie);
        this.title_force = (TextView)findViewById(R.id.title_force);
        this.force_plus = (Button)findViewById(R.id.force_plus);
        this.force = (TextView)findViewById(R.id.force);
        this.force_moins = (Button)findViewById(R.id.force_moins);
        this.title_agilite = (TextView)findViewById(R.id.title_agilite);
        this.agilite_plus = (Button)findViewById(R.id.agilite_plus);
        this.agilite = (TextView)findViewById(R.id.agilite);
        this.agilite_moins = (Button)findViewById(R.id.agilite_moins);
        this.title_charisme = (TextView)findViewById(R.id.title_charisme);
        this.charisme_plus = (Button)findViewById(R.id.charisme_plus);
        this.charisme = (TextView)findViewById(R.id.charisme);
        this.charisme_moins = (Button)findViewById(R.id.charisme_moins);
        this.title_sexe = (TextView)findViewById(R.id.title_sexe);
        this.sexe = (TextView)findViewById(R.id.sexe);
        this.set_stats = getSharedPreferences("stats.txt", MODE_PRIVATE);
        this.parametres = getSharedPreferences("parametres.saucisse", MODE_PRIVATE);
        this.force_plus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) { SetStatsActivity.this._modify_stats(-1.0D, 1.0D, 0.0D, 0.0D); }
        });
        this.force_moins.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) { SetStatsActivity.this._modify_stats(1.0D, -1.0D, 0.0D, 0.0D); }
        });
        this.agilite_plus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) { SetStatsActivity.this._modify_stats(-1.0D, 0.0D, 1.0D, 0.0D); }
        });
        this.agilite_moins.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) { SetStatsActivity.this._modify_stats(1.0D, 0.0D, -1.0D, 0.0D); }
        });
        this.charisme_plus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) { SetStatsActivity.this._modify_stats(-1.0D, 0.0D, 0.0D, 1.0D); }
        });
        this.charisme_moins.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) { SetStatsActivity.this._modify_stats(1.0D, 0.0D, 0.0D, -1.0D); }
        });
        this.sexe.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                if (SetStatsActivity.this.set_stats.getString("Sexe", "").equals("1")) {
                    SetStatsActivity.this.set_stats.edit().putString("Sexe", "2").commit();
                } else {
                    SetStatsActivity.this.set_stats.edit().putString("Sexe", "1").commit();
                }
                SetStatsActivity.this._actualiser();
            }
        });
    }

    private void initializeLogic() { _actualiser(); }

    private void showMessage(String paramString) { Toast.makeText(getApplicationContext(), paramString, Toast.LENGTH_SHORT).show(); }

    public ArrayList<Double> getCheckedItemPositionsToArray(ListView paramListView) {
        ArrayList arrayList = new ArrayList();
        SparseBooleanArray sparseBooleanArray = paramListView.getCheckedItemPositions();
        for (byte b = 0;; b++) {
            if (b >= sparseBooleanArray.size())
                return arrayList;
            if (sparseBooleanArray.valueAt(b))
                arrayList.add(Double.valueOf(sparseBooleanArray.keyAt(b)));
        }
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.set_stats);
        initialize();
        initializeLogic();
    }
}
