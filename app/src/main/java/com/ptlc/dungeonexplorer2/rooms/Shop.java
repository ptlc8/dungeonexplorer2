package com.ptlc.dungeonexplorer2.rooms;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.ptlc.dungeonexplorer2.Player;
import com.ptlc.dungeonexplorer2.ShopActivity;
import com.ptlc.dungeonexplorer2.render.Card;

public class Shop extends Room {

    private String title, description;
    private Card leftButton, rightButton;
    private boolean isEnded = false;

    private final Context applicationContext;
    private Player player;

    public Shop(Context applicationContext, Player player) {
        super("Magasin");
        this.applicationContext = applicationContext;
        this.player  = player;

        title = "Une boutique";
        leftButton = Card.Trade;
        rightButton = Card.Continue;
        int rdm = getRandom(1, 3);
        if (rdm == 1) description = "Personne à l'horizon";
        if (rdm == 2) description = "Quelqu'un fouille dans la boutique";
        if (rdm == 3) description = "Une lumière forte en jaillit";
        rdm = getRandom(1, 3);
        if (rdm == 1) description += ", personne au comptoir peut-être un nain ? P";
        if (rdm == 2) description += ", un chat sur le comptoir en tout cas p";
        if (rdm == 3) description += ", une pancarte immobile est gravée d'une fiole noire, p";
        rdm = getRandom(1, 3);
        if (rdm == 1) description += "as de doute c'est un repère de malfrats.";
        if (rdm == 2) description += "as de doute c'est une ancienne office de tourisme elfique.";
        if (rdm == 3) description += "as de doute c'est une brasserie-boutique de type 138 !";
        }

    @Override
    public void onAction1() { // marchander
        Intent intent = new Intent()
            .putExtra("player", new Gson().toJson(player))
            .setClass(applicationContext, ShopActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        applicationContext.startActivity(intent);
    }

    @Override
    public void onAction2() { // continuer
        isEnded = true;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Card getLeftCard() {
        return leftButton;
    }

    @Override
    public Card getRightCard() {
        return rightButton;
    }

    @Override
    public boolean nextRoom() {
        return isEnded;
    }
}
