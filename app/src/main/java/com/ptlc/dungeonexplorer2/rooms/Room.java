package com.ptlc.dungeonexplorer2.rooms;

import com.ptlc.dungeonexplorer2.render.Card;

import java.util.Random;

public abstract class Room {

    private final String name;

    protected Room(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void onAction1();
    public abstract void onAction2();

    public abstract String getTitle();
    public abstract String getDescription();
    public abstract Card getLeftCard();
    public abstract Card getRightCard();

    public abstract boolean nextRoom();

    protected static int getRandom(int minValue, int maxValue) {
        return new Random().nextInt((maxValue - minValue) + 1) + minValue;
    }

}
