package com.ptlc.dungeonexplorer2.rooms;

import com.ptlc.dungeonexplorer2.render.Card;

public class Corridor extends  Room {

    private String title, description;
    private Card leftButton, rightButton;
    private boolean isEnded = false;

    public Corridor() {
        super("Couloir");

        title = "Un couloir";
        int rdm = getRandom(1, 3);
        if (rdm == 1)
            description = "Au sol des traces de sang, ";
        if (rdm == 2)
            description = "Deux torches sont accrochés aux murs, ";
        if (rdm == 3)
            description = "Quelques chauve-souris au plafond, ";
        rdm = getRandom(1, 3);
        if (rdm == 1) {
            description += "à gauche une porte, en face une autre porte, ";
            leftButton = Card.Left;
            rightButton = Card.Front;
        }
        if (rdm == 2) {
            description += "en face une porte, à droite une autre porte, ";
            leftButton = Card.Front;
            rightButton = Card.Right;
        }
        if (rdm == 3) {
            description += "à gauche une porte, à droite une autre porte, ";
            leftButton = Card.Left;
            rightButton = Card.Right;
        }
        description += "où choisissez-vous d'aller ?";
    }

    @Override
    public void onAction1() {
        isEnded = true;
    }

    @Override
    public void onAction2() {
        isEnded = true;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Card getLeftCard() {
        return leftButton;
    }

    @Override
    public Card getRightCard() {
        return rightButton;
    }

    @Override
    public boolean nextRoom() {
        return isEnded;
    }
}
