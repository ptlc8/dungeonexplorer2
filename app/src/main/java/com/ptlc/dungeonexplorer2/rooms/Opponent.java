package com.ptlc.dungeonexplorer2.rooms;

import com.ptlc.dungeonexplorer2.Item;

import java.util.Random;

public class Opponent {

    public static final int IN_ROOM = 1, IN_CHEST = 2;

    private String definedName, undefinedName;
    private int health, strength;
    private Item weapon = null;

    public Opponent(int spawn, int score) {
        int rdm;
        if (spawn == IN_ROOM) {
            rdm = getRandom(1, 3); // definition de la race
            if (rdm == 1) {
                this.undefinedName = "un squelette ";
                this.definedName = "le squelette ";
                this.health = getRandom(4, 12);
                this.strength = getRandom(2, 6);
            } else if (rdm == 2) {
                this.undefinedName = "un gobelin-poulet ";
                this.definedName = "le gobelin-poulet ";
                this.health = getRandom(8, 18);
                this.strength = getRandom(3, 4);
            } else if (rdm == 3) {
                this.undefinedName = "un troll ";
                this.definedName = "le troll ";
                this.health = getRandom(10, 22);
                this.strength = getRandom(4, 9);
            }
            rdm = getRandom(1, 3); // definition de l'arme
            if (rdm == 1) {
                this.undefinedName += "sans arme ";
                this.definedName += "sans arme ";
                this.weapon = Item.rien;
            } else if (rdm == 2) {
                this.undefinedName = this.undefinedName.concat("muni d'un couteau ");
                this.definedName = this.definedName.concat("muni d'un couteau ");
                this.strength += 2;
                this.weapon = Item.couteau;
            } else if (rdm == 3) {
                this.undefinedName = this.undefinedName.concat("armé d'une masse ");
                this.definedName = this.definedName.concat("armé d'une masse ");
                this.strength += 4;
                this.weapon = Item.couteau;
            }
            for (int buff = 0; buff < ((int) (score / 20)); buff++) { // amelioration selon le score
                this.health += 1;
                this.strength += 1;
            }
        } else if (spawn == IN_CHEST) {
            rdm = getRandom(1, 2); // definition de la race
            if (rdm == 1) {
                this.undefinedName = "un gobelin ";
                this.definedName = "le gobelin ";
                this.health = getRandom(4, 12);
                this.strength = getRandom(2, 6);
                rdm = getRandom(1, 3); // definition de l'arme du gobelin
                if (rdm == 1) {
                    this.weapon = Item.rien;
                } else if (rdm == 2) {
                    this.undefinedName += "muni d'un couteau ";
                    this.definedName += "muni d'un couteau ";
                    this.strength += 2;
                    this.weapon = Item.couteau;
                }
            } else if (rdm == 2) {
                this.undefinedName = "un slime ";
                this.definedName = "le slime ";
                this.health = getRandom(8, 18);
                this.strength = getRandom(1, 3);
            }
            for (int buff = 0; buff < ((int) (score / 20)); buff++) {
                this.health += 1;
                this.strength += 1;
            }
        }
        this.undefinedName += "(" + this.health + "; " + this.strength + ") ";
        this.definedName += "(" + this.health + "; " + this.strength + ") ";
    }

    public Item getLoot() {
        int rdm = getRandom(1, 12);
        if (0 < rdm && rdm < 4) return null;
        if (3 < rdm && rdm < 7) return getWeapon();
        if (6 < rdm && rdm < 9) return Item.baton;
        if (rdm == 9) return Item.pieces_d_or;
        if (rdm == 10) return Item.cle;
        if (rdm == 11) return Item.talkie_walkie;
        if (rdm == 12) return Item.amulette_bleue;
        return getWeapon();
    }

    public String getUndefinedName() {
        return undefinedName;
    }

    public String getDefinedName() {
        return definedName;
    }

    public int getHealth() {
        return health;
    }

    public void takeDamage(int damage) {
        health -= damage;
    }

    public int getStrength() {
        return strength;
    }
    public int getDamage() {
        return strength + (weapon != null ? weapon.getDamage() : 0);
    }

    public Item getWeapon() {
        return weapon;
    }

    private int getRandom(int minValue, int maxValue) {
        return new Random().nextInt((maxValue - minValue) + 1) + minValue;
    }

}
