package com.ptlc.dungeonexplorer2.rooms;

import com.ptlc.dungeonexplorer2.render.Card;

public class Stairs extends Room {


    private String title, description;
    private Card leftButton, rightButton;
    private boolean isEnded = false;

    public Stairs() {
        super("Escaliers");

        int rdm = getRandom(1, 2);
        if (rdm == 1) title = "Des escaliers montants";
        if (rdm == 2) title = "Des escaliers descendants";
        rdm = getRandom(1, 3);
        if (rdm == 1) description = "Malgré le manque de lumière, ";
        if (rdm == 2) description = "Ormis les trous dans les murs, ";
        if (rdm == 3) description = "Pas un seul être vivant, ";
        rdm = getRandom(1, 3);
        if (rdm == 1) description += "rien d'intéressant.";
        if (rdm == 2) description += "on pourrait presque y planter une tente.";
        if (rdm == 3) description += "on pourrait se poser 5 minutes.";
        leftButton = Card.Continue;
        rightButton = Card.Continue;
    }

    @Override
    public void onAction1() {
        isEnded = true;
    }

    @Override
    public void onAction2() {
        isEnded = true;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Card getLeftCard() {
        return leftButton;
    }

    @Override
    public Card getRightCard() {
        return rightButton;
    }

    @Override
    public boolean nextRoom() {
        return isEnded;
    }

}