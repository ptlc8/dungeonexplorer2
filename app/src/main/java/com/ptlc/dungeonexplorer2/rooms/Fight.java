package com.ptlc.dungeonexplorer2.rooms;

import com.ptlc.dungeonexplorer2.Item;
import com.ptlc.dungeonexplorer2.Player;

import java.util.Random;

public abstract class Fight {

    private final Player player;
    private final Opponent opponent;
    private String description;

    Fight(Player player, Opponent opponent) {
        this.player = player;
        this.opponent = opponent;
        this.description = "";
    }

    private void opponentAttack() {
        if (opponent.getHealth() < player.getHealth() && new Random().nextInt(16) <= player.getCharism()) {
            description += ", mais elle décide lâchement de fuir.";
            onOpponentRunAway(); // ok
            return; // fuite de la creature
        }
        player.takeDamage(opponent.getDamage() - player.getProtection());
        int rdm = new Random().nextInt(4);
        if (rdm == 1)
            description += ", elle vous attaque à son tour et vous inflige " + (opponent.getDamage() - player.getProtection()) + " dégâts";
        else if (rdm == 2)
            description += ", elle se défend et vous fait perdre " + (opponent.getDamage() - player.getProtection()) + " ❤";
        else if (rdm == 3)
            description += ", elle s'attaque à vous et vous met " + (opponent.getDamage() - player.getProtection()) + " dégâts dans la figure";
        if (player.getHealth() <= 0) {
            description += ", vous mourez de ce dernier coup et c'est ainsi que s'achève votre épopée. ✝";
            onPlayerDeath();
            return; // mort du joueur
        }
        playerAttack(false);
    }

    void playerAttack() {playerAttack(true);}
    private void playerAttack(boolean isFirst) {
        opponent.takeDamage(player.getDamage());
        if (isFirst) description = "Vous attaquer la créature";
        else {
            int rdm = new Random().nextInt(4);
            if (rdm == 1) description += ", vous lui remettez un coup";
            if (rdm == 2) description += ", vous vous battez aussi";
            if (rdm == 3) description += ", vous lui sautez dessus de toutes vos forces";
        }
        if (opponent.getHealth() <= 0) {
            description += " et " + opponent.getDefinedName() + "s'écroule à vos pieds.";
            onOpponentDeath(); // ok
            return; // mort de la créature
        }
        opponentAttack();
    }

    void playerDodge() {
        if (new Random().nextInt(16) <= player.getAgility()) {
            description = "Vous réussissez à vous en tirer sans égratinure.";
            onPlayerDodge();
            return; // esquive du joueur
        }
        description = "Vous arrivez à atteindre la porte, mais " + opponent.getDefinedName() + "a réussi à vous mettre un coup.";
        player.takeDamage(opponent.getDamage() / 3 * 2 - player.getProtection());
        //showMessage("Vie : -" + ((ennemi.getDamage() / 3 * 2) -  player.getProtection()) + " ❤"); // TODO
        if (player.getHealth() <= 0) {
            description += ", vous en mourez. ✝";
            onPlayerDeath();
            return; // mort lors d'un echec d'esquive
        }
        description += ".";
        onPlayerDodge();
    }

    void loot() {
        Item loot = opponent.getLoot();
        description = "Vous fouillez " + opponent.getDefinedName() + "et vous obtenez : " + (loot.getName()!=null ? loot.getName() : "rien") + ".";
        player.give(loot, 1);
        onPlayerLoot();
    }

    public abstract void onOpponentDeath(); // fouille
    public abstract void onOpponentRunAway(); // fin de la salle ou inspection
    public abstract void onPlayerDodge(); // fin de la salle
    public abstract void onPlayerDeath(); // fin de la partie
    public abstract void onPlayerLoot(); // fin de la salle ou inspection

    Opponent getOpponent() {
        return opponent;
    }

    String getFightDescription() {
        return description;
    }

}
