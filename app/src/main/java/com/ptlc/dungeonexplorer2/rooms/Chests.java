package com.ptlc.dungeonexplorer2.rooms;

import com.ptlc.dungeonexplorer2.Item;
import com.ptlc.dungeonexplorer2.Player;
import com.ptlc.dungeonexplorer2.State;
import com.ptlc.dungeonexplorer2.render.Card;

import java.util.Random;

public class Chests extends Room {

    private String title, description;
    private Card leftCard, rightCard;
    private boolean isEnded = false;
    private int state;

    private final Player player;
    private final int score;
    private Chest coffreG, coffreD;
    private Fight fight;

    public Chests(Player player, int score) {
        super("Coffres");
        this.player = player;
        this.score = score;
        state = State.DEUX_COFRRES;

        title = "Une salle lumineuse";
        int rdm = getRandom(1, 3);
        if (rdm == 1) description = "Deux coffres sont face à vous, ";
        if (rdm == 2) description = "En face de vous deux coffres à l'allure banale, ";
        if (rdm == 3) description = "Deux coffres vous font face, ";
        rdm = getRandom(1, 3);
        if (rdm == 1) description += "seul un pourra être ouvert, v";
        if (rdm == 2) description += "un seul vous ne pourrez choisir, v";
        if (rdm == 3)
            description += "trois possibilités s'offrent à vous : celui de gauche, celui de droite ou aucun. V";
        description += "oulez-vous tentez ?";
        leftCard = Card.Try;
        rightCard = Card.Ignore;
    }

    @Override
    public void onAction1() {
        switch (state) {
            case State.DEUX_COFRRES:
                coffreG = Chest.getRandom();
                coffreD = Chest.getRandom();
                description = "À gauche ";
                description += coffreG.getUndefinedName() + ", ";
                description += "à droite ";
                description += coffreD.getUndefinedName() + ", ";
                description += "lequel choisissez-vous ?";
                leftCard = Card.Left;
                rightCard = Card.Right;
                state = State.CHOOSE_CHEST;
                break;
            case State.CHOOSE_CHEST:
                openChest(coffreG);
                state = State.ENDING_ROOM;
                break;
            case State.ENDING_ROOM:
                isEnded = true;
                break;
            case State.IN_FIGHT:
                fight.playerAttack();
                break;
            case State.LOOTING:
                fight.loot();
                break;
        }
    }

    @Override
    public void onAction2() {
        switch (state) {
            case State.DEUX_COFRRES:
                description = "Dommage, la prochaine fois peut-être...";
                leftCard = Card.Continue;
                rightCard = Card.Continue;
                state = State.ENDING_ROOM;
                break;
            case State.CHOOSE_CHEST:
                openChest(coffreD);
                break;
            case State.LOOTING:
            case State.ENDING_ROOM:
                isEnded = true;
                break;
            case State.IN_FIGHT:
                fight.playerDodge();
                break;
        }
    }

    private void openChest(Chest chest) {
        if (getRandom(1, 10) > 6) {
            fight = new Fight(player, new Opponent(Opponent.IN_CHEST, score)) {
                public void onOpponentDeath() {
                    description = getDescription();
                    leftCard = Card.Ransack;
                    rightCard = Card.Ignore;
                    state = State.LOOTING;
                }
                public void onOpponentRunAway() {
                    description = getFightDescription();
                    leftCard = Card.Continue;
                    rightCard = Card.Continue;
                    state = State.ENDING_ROOM;
                }
                public void onPlayerDodge() {
                    description = getFightDescription();
                    leftCard = Card.Continue;
                    rightCard = Card.Continue;
                    state = State.ENDING_ROOM;
                }
                public void onPlayerDeath() {
                    description = getFightDescription();
                    // nothing ?
                }
                public void onPlayerLoot() {
                    description = getFightDescription();
                    leftCard = Card.Continue;
                    rightCard = Card.Continue;
                    state = State.ENDING_ROOM;
                }
            };
            description = "Vous ouvrez le coffre et à votre grande surprise, " + fight.getOpponent().getUndefinedName() + "vous saute dessus et vous inflige " + fight.getOpponent().getStrength() + "❤";
            player.takeDamage(fight.getOpponent().getStrength());
            if (player.getHealth() < 1) {
                description += ", vous mourez sur le coup. Votre aventure se termine ici. ✝";
                return;
            }
            description += ".";
            leftCard = Card.Attack;
            rightCard = Card.RunAway;

            state = State.IN_FIGHT;
            return;
        }
        Item loot = chest.getLoot();
        description = "Vous ouvrez " + chest.getDemonstrativeName() + " et vous y trouvez : " + loot.getName();
        player.give(loot, 1);
        leftCard = Card.Continue;
        rightCard = Card.Continue;
        state = State.ENDING_ROOM;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Card getLeftCard() {
        return leftCard;
    }

    @Override
    public Card getRightCard() {
        return rightCard;
    }

    @Override
    public boolean nextRoom() {
        return isEnded;
    }

    public enum Chest implements Item.Loot {

        LONG("ce long coffre", "un coffre assez long", Item.baton, Item.barre_de_fer, Item.machette, null),
        BOMBED("ce coffre bombé", "un coffre plutôt bombé", Item.fiole_vide, Item.fiole_verte, Item.fiole_rouge, Item.saphir, null),
        HERMETIC("ce coffre hemétique", "un coffre apparement hermétique", Item.poudre_verte, Item.pieces_d_or, null, Item.poudre_lumineuse, Item.eau_benite);

        private static Chest getRandom() {
            return values()[new Random().nextInt(values().length)];
        }

        private final String demonstrativeName, undefinedName;
        private final Item[] loots;

        Chest(String demonstrativeName, String undefinedName, Item... loots) {
            this.demonstrativeName = demonstrativeName;
            this.undefinedName = undefinedName;
            this.loots = loots;
        }

        @Override
        public Item getLoot() {
            return loots[new Random().nextInt(loots.length)];
        }

        public String getDemonstrativeName() {
            return demonstrativeName;
        }

        public String getUndefinedName() {
            return undefinedName;
        }

    }

}
