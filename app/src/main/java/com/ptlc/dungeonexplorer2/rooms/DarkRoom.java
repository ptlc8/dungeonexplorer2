package com.ptlc.dungeonexplorer2.rooms;

import com.ptlc.dungeonexplorer2.Item;
import com.ptlc.dungeonexplorer2.Player;
import com.ptlc.dungeonexplorer2.State;
import com.ptlc.dungeonexplorer2.render.Card;

public class DarkRoom extends Room {

    private String title, description;
    private Card leftButton, rightButton;
    private boolean isEnded = false;
    public int state; // TODO : private

    private Player player;
    private int score;
    private Fight fight = null;
    private static int vieux_fou = 0; // static ???

    public DarkRoom(Player player, int score) {
        super("Salle sombre");
        this.player = player;
        this.score = score;

        title = "Une salle";
        int rdm;
        if (getRandom(0, 100) != 1 || score <= 20 || vieux_fou == -1) {
            rdm = getRandom(1, 7);
            if (rdm <= 3) {
                rdm = getRandom(1, 3);
                if (rdm == 1) description = "Apparemment personne, une porte en face.";
                if (rdm == 2) description = "Pas l'ombre d'un poulet.";
                if (rdm == 3) description = "Pas de cadavre, rien de bien intéressant.";
                leftButton = Card.Continue;
                rightButton = Card.Inspect;
                state = State.NOTHING_IN_ROOM;
            } else if (rdm <= 6) {
                Opponent ennemi = new Opponent(Opponent.IN_ROOM, score);
                setFight(ennemi);
                description = ennemi.getUndefinedName();
                rdm = getRandom(1, 3);
                if (rdm == 1) {
                    description += "apparaît et vous fait face, ";
                    leftButton = Card.Attack;
                    rightButton = Card.RunAway;
                    state = State.IN_FIGHT;
                }
                if (rdm == 2) {
                    description += "semble inerte sur le sol, ";
                    leftButton = Card.Ransack;
                    rightButton = Card.Ignore;
                    state = State.OPPONENT_ON_GROUND;
                }
                if (rdm == 3) {
                    description += "est tapi dans un coin, ";
                    leftButton = Card.Attack;
                    rightButton = Card.Ignore;
                    state = State.OPPONENT_IN_CORNER;
                }
                description += "que faites-vous ?";
            } else if (rdm == 7) {
                description = "Un poulet ";
                rdm = getRandom(1, 3);
                if (rdm == 1) description += "apparaît et vous fait face, ";
                if (rdm == 2) description += "semble inerte sur le sol, ";
                if (rdm == 3) description += "est tapi dans un coin, ";
                leftButton = Card.Ignore;
                rightButton = Card.Attack;
                state = State.POULET;
                description += "que faites-vous ?";
                return;
            }
            return;
        }
        description = "Vous entrez dans une nouvelle salle et un viel homme vous fait barrage en vous criant «Vous ne passerez pas !», que faites-vous ?";
        leftButton = Card.Attack;
        rightButton = Card.RunAway;
        state = State.VIEUX_FOU;
        vieux_fou++;
    }

    @Override
    public void onAction1() {
        switch (state) {
            case State.NOTHING_IN_ROOM:
            case State.ENDING_ROOM:
            case State.CANT_OPEN_CHEST:
                isEnded = true;
                break;
            case State.IN_FIGHT:
            case State.OPPONENT_IN_CORNER:
            case State.OPPONENT_IN_FRONT:
                fight.playerAttack();
                break;
            case State.OPPONENT_ON_GROUND:
            case State.INANIMATED_OPPONENT:
                if (getRandom(1, 2) == 1) {
                    Item loot = fight.getOpponent().getLoot();
                    description = "Vous fouillez " + fight.getOpponent().getDefinedName() + "et il ne bouge pas, vous obtenez : " + (loot==null ? "rien" : loot.getName()) + ".";
                    player.give(loot, 1);
                    leftButton = Card.Continue;
                    rightButton = Card.Inspect;
                    state = State.NOTHING_IN_ROOM;
                } else {
                    description = "Vous fouillez la créature, mais " + fight.getOpponent().getDefinedName() + "se lève et vous attaque, que faites-vous ?";
                    leftButton = Card.Attack;
                    rightButton = Card.RunAway;
                    state = State.IN_FIGHT;
                }
                break;
            case State.POULET:
                if (getRandom(1, 2) == 1) {
                    description = "Le poulet perd espoir, et court se cacher dans une fissure dans un mur, que faites-vous ?";
                    leftButton = Card.Continue;
                    rightButton = Card.Inspect;
                    state = State.NOTHING_IN_ROOM;
                    break;
                }
                int damage = (getRandom(1, 2)) - player.getProtection();
                if (damage < 0) damage = 0;
                player.takeDamage(damage);
                if (player.getHealth() > 0) {
                    description = "Le poulet s'attaque à vous, que faites-vous ?";
                    leftButton = Card.Ignore;
                    rightButton = Card.Attack;
                    state = State.POULET;
                    break;
                }
                description = "Le poulet s'attaque à vous, il réussit à vous terminer, vous êtes donc mort de la pire façon possible. ✝";
                /*if (!achievements.getString("pire_mort", "").equals("1")) {
                    achievements.edit().putString("pire_mort", "1").apply();
                    showMessage("La pire mort   ✔");
                }*/
                break;
            case State.OPEN_CHEST:
                if (getRandom(1, 2) == 1) {
                    Item treasure = Item.Chest.LOCKED_CHEST.getLoot();
                    if (player.has(Item.cle, 1)) {
                        description = "Le coffre est muni d'une serrure, vous possédez une clé, vous obtenez : " + (treasure!=null ? treasure.getName() : "rien") + ".";
                        player.give(treasure, 1);
                        player.remove(Item.cle, 1);
                        //_refresh_inventory(); TODO
                        leftButton = rightButton = Card.Continue;
                        state = State.ENDING_ROOM;
                    } else {
                        description = "Le coffre est muni d'une serrure, vous n'avez pas de clé, que faites-vous ?";
                        leftButton = Card.Ignore;
                        rightButton = Card.Force;
                        state = State.CANT_OPEN_CHEST; // TODO
                    }
                } else {
                    Item treasure = Item.Chest.NORMAL_CHEST.getLoot();
                    description = "Le coffre s'ouvre, vous y trouvez : " + (treasure!=null ? treasure.getName() : "rien") + ".";
                    player.give(treasure, 1);
                    leftButton = rightButton = Card.Continue;
                    state = State.ENDING_ROOM;
                }
                break;
            case State.VIEUX_FOU:
                description = "Vous attaquez le vieux fou, il s'écroule comme un tas d'os et disparait dans un petit panache de fumée blanche. Vous obtenez : Bâton.";
                vieux_fou = -1;
                player.give(Item.baton, 1);
                leftButton = Card.Continue;
                leftButton = Card.Inspect;
                state = State.NOTHING_IN_ROOM;
                break;
            case State.LOOTING:
                fight.loot();
                break;
        }
    }

    @Override
    public void onAction2() {
        int rdm;
        switch (state) {
            case State.NOTHING_IN_ROOM: // inspecter
                rdm = Item.fiole_lumineuse_verte.equals(player.getTool()) ? getRandom(3, 10) : getRandom(1, 9);
                if (rdm <= 4) {
                    description = "Vous n'avez rien trouvé.";
                    leftButton = Card.Continue;
                    rightButton = Card.Continue;
                    state = State.ENDING_ROOM;
                } else if (rdm <= 6) {
                    setFight(new Opponent(Opponent.IN_ROOM, score));
                    description = "Après quelques recherches, " + fight.getOpponent().getUndefinedName() + "apparaît à vous, que faites-vous ?";
                    leftButton = Card.Attack;
                    rightButton = Card.RunAway;
                    state = State.OPPONENT_IN_FRONT;
                } else {
                    description = "Un coffre apparaît à vous" + (rdm >= 10 ? " grâce à votre lueur verte" : "") + ", que faites-vous ?";
                    leftButton = Card.Open;
                    rightButton = Card.Ignore;
                    state = State.OPEN_CHEST;
                }
                break;
            case State.OPEN_CHEST: // Ignorer
                description = "Dommage...";
                leftButton = rightButton = Card.Continue;
                state = State.ENDING_ROOM;
                break;
            case State.IN_FIGHT: // esquiver
            case State.OPPONENT_IN_FRONT:
                fight.playerDodge(); // TODO : 1 freeze ?
                break;
            case State.OPPONENT_IN_CORNER:
                rdm = getRandom(1, 3);
                if (rdm == 1) {
                    description = "Malgré le bruit que vous faites, " + fight.getOpponent().getDefinedName() + "ne bronche pas, que faites-vous ?";
                    leftButton = Card.Ransack;
                    rightButton = Card.Ignore;
                    state = State.INANIMATED_OPPONENT;
                } else if (rdm == 2) {
                    description = "Vous faites du bruit, " + fight.getOpponent().getDefinedName() + "se met à respirer fortement, que faites-vous ?";
                    leftButton = Card.Continue;
                    rightButton = Card.Inspect;
                    state = State.NOTHING_IN_ROOM;
                } else if (rdm == 3) {
                    description = "Vous faites un sacré boucan, " + fight.getOpponent().getDefinedName() + "se lève et vous attaque, que faites-vous ?";
                    leftButton = Card.Attack;
                    rightButton = Card.RunAway;
                    state = State.OPPONENT_IN_FRONT;
                }
                break;
            case State.POULET:
                description = "Le poulet meurt au premier coup, que faites-vous ?";
                player.give(Item.viande_crue, 1);
                player.give(Item.plume, getRandom(0, 2));
                leftButton = Card.Continue;
                rightButton = Card.Inspect;
                state = State.NOTHING_IN_ROOM;
                break;
            case State.VIEUX_FOU:
                description = "Le vieux fou à du mal à marcher a cause de son mal de dos, vous l'esquivez sans problème.";
                leftButton = rightButton = Card.Continue;
                state = State.ENDING_ROOM;
                break;
            case State.LOOTING:
            case State.INANIMATED_OPPONENT:
            case State.OPPONENT_ON_GROUND:
                state = State.NOTHING_IN_ROOM;
                leftButton = Card.Continue;
                rightButton = Card.Inspect;
                break;
            case State.CANT_OPEN_CHEST:
                Item treasure = Item.Chest.LOCKED_CHEST.getLoot();
                if (getRandom(1, 4) == 1) {
                    description = "Vous réussissez à forcer le coffre, vous y trouvez : " + (treasure!=null ? treasure.getName() : "rien") + ".";
                    player.give(treasure, 1);
                } else {
                    description = "Vous ne réussissez pas à forcer le coffre.";
                }
                leftButton = rightButton = Card.Continue;
                state = State.ENDING_ROOM;
                break;
            case State.ENDING_ROOM:
                isEnded = true;
                break;
        }
    }

    private void setFight(Opponent opponent) {
        fight = new Fight(player, opponent) {
            @Override
            public void onOpponentDeath() {
                description = getFightDescription();
                leftButton = Card.Ransack;
                rightButton = Card.Ignore;
                state = State.LOOTING;
            }
            @Override
            public void onOpponentRunAway() {
                description = getFightDescription();
                leftButton = Card.Continue;
                rightButton = Card.Inspect;
                state = State.NOTHING_IN_ROOM;
            }
            @Override
            public void onPlayerDodge() {
                description = getFightDescription();
                leftButton = Card.Continue;
                rightButton = Card.Continue;
                state = State.ENDING_ROOM;
            }
            @Override
            public void onPlayerDeath() {
                description = getFightDescription();
                // nothing ?
            }
            @Override
            public void onPlayerLoot() {
                description = getFightDescription();
                leftButton = Card.Continue;
                rightButton = Card.Inspect;
                state = State.NOTHING_IN_ROOM;
            }
        };
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Card getLeftCard() {
        return leftButton;
    }

    @Override
    public Card getRightCard() {
        return rightButton;
    }

    @Override
    public boolean nextRoom() {
        return isEnded;
    }
}
