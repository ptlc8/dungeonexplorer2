package com.ptlc.dungeonexplorer2;

public class State {
    public static final int DEAD = -1; //
    public static final int START = 0; // Entrer | Sortir
    @Deprecated
    public static final int COULOIR = 1; // ou escaliers // Continuer | Continuer // remplacer par ENDING_ROOM
    public static final int ALIVE = 2;
    public static final int ENDING_ROOM = 5; // Continuer | Continuer
    public static final int NOTHING_IN_ROOM = 20; // ou loot_mob ou mob_ran_away // Continuer | Inspecter
    @Deprecated
    public static final int LOOT_CHEST = 21; // ou cant_force_chest // lors de l'affichage des loots d'un coffres // remplacer par ENDING_ROOM
    public static final int OPEN_CHEST = 22; // Ouvrir | Ignorer
    public static final int CANT_OPEN_CHEST = 23; // Ignorer | Forcer
    public static final int LOOTING = 25; // Fouiller | Ignorer
    public static final int IN_FIGHT = 30; // Attaquer | Esquiver
    public static final int OPPONENT_IN_FRONT = 31; // Attaquer | Esquiver
    public static final int OPPONENT_ON_GROUND = 32; // Fouiller | Ignorer // Risque de réveil
    public static final int OPPONENT_IN_CORNER = 33; // Attaquer | Ignorer
    public static final int OPPONENT_DEATH = 35;
    public static final int INANIMATED_OPPONENT = 36; // Fouiller | Ignorer
    public static final int POULET = 40;
    public static final int MAGASIN = 50; // Marchander | Continuer // TODO
    public static final int DEUX_COFRRES = 60;
    public static final int CHOOSE_CHEST = 61;
    public static final int VIEUX_FOU = 70;
}
