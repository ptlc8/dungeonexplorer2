package com.ptlc.dungeonexplorer2;

import com.ptlc.dungeonexplorer2.rooms.Chests;

import java.util.Random;

public enum Item {

    rien("rien (@Deprecated ?)", "", "0", 0),
    viande_crue("Viande crue", "", "0", 0),
    plume("Plume", "", "0", 0),
    cle("Clé", "", "0", 0),
    fiole_rouge("Fiole rouge", "", "0", 0),
    fiole_vide("Fiole vide", "", "0", 0),
    poudre_verte("Poudre verte", "", "0", 0),
    casque_en_amethyste("Casque en améthyste", "", "0", 2),
    baton("Bâton", "", "1", 0),
    couteau("Couteau", "", "2", 0),
    amulette_bleue("Amulette bleue", "", "0", 0),
    talkie_walkie("Talkie Walkie", "", "0", 0),
    viande_cuite("Viande cuite", "", "0", 0),
    boite_de_cookies("Boîte de cookies", "", "0", 0),
    baton_magique_bleu("Bâton magique bleu", "", "2~8", 0),
    oreiller("Oreiller", "", "0", 0),
    fiole_verte("Fiole verte", "", "0", 0),
    fiole_jaune("Fiole jaune", "", "0", 0),
    amulette_verte("Amulette verte", "", "0", 0),
    masse("Masse", "", "4", 0),
    poudre_lumineuse("Poudre lumineuse", "", "0", 0),
    prisme_noir("Prisme noir", "", "0", 0),
    rubis("Rubis", "", "0", 0),
    epee_elfique_1("Épée elfique Λ", "", "3~5", 0),
    diamant("Diamant", "", "0", 0),
    barre_de_fer("Barre de fer", "", "2", 0),
    machette("Machette", "", "3", 0),
    saphir("Saphir", "", "0", 0),
    eau_benite("Eau bénite", "", "0", 0),
    baton_magique_vert("Bâton magique vert", "", "3~9", 0),
    baton_magique_rouge("Bâton magique rouge", "", "0~9", 0),
    fiole_lumineuse_verte("Fiole lumineuse verte", "", "0", 0),
    pieces_d_or("Pièces d'or", "", "0", 0);

    public static Item get(String name) {
        try {
            return valueOf(name);
        } catch (Exception e) {
            return null;
        }
    }

    private final String name;
    private final String description;
    private final String damage;
    private final int protection;

    Item(String name, String description, String damage, int protection) {
        this.name = name;
        this.description = description;
        this.damage = damage;
        this.protection = protection;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getDamage() {
        if (!damage.contains("~"))
            return Integer.parseInt(damage);
        String[] values = damage.split("~");
        int min = Integer.parseInt(values[0]), max = Integer.parseInt(values[1]);
        return new Random().nextInt(max - min) + min;
    }

    public int getProtection() {
        return protection;
    }

    // ----

    public interface Loot {
        Item getLoot();
    }

    public enum Chest implements Loot {

        NORMAL_CHEST(null, Item.cle, Item.poudre_lumineuse, Item.fiole_vide, Item.poudre_verte, Item.pieces_d_or),
        LOCKED_CHEST(Item.cle, Item.fiole_rouge, Item.prisme_noir, Item.casque_en_amethyste, Item.pieces_d_or, Item.rubis, Item.diamant);

        private final Item[] loots;

        Chest(Item... loots) {
            this.loots = loots;
        }

        @Override
        public Item getLoot() {
            return loots[new Random().nextInt(loots.length)];
        }

    }

}
