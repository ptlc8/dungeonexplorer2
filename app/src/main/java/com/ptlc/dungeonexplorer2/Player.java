package com.ptlc.dungeonexplorer2;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Player {

    private int health, strength, agility, charism;
    private Sex sex;
    public enum Sex {M, F;}

    private Map<Item, Integer> inventory = new HashMap<>();
    private int money = 0;
    private Item weapon = null;
    private Item protection1 = null;
    private Item tool = null;

    public Player(int health, int strength, int agility, int charism, Sex sex, Item weapon) {
        for (Item item : Item.values()) inventory.put(item, 0);
        this.health = health;
        this.strength = strength;
        this.agility = agility;
        this.charism = charism;
        this.weapon = weapon;
    }

    public void give(Item item, int amount) {
        if (item == null) return;
        if (item.equals(Item.pieces_d_or))
            money += (new Random().nextInt(4) + 4) * amount;
        else
            inventory.put(item, (inventory.get(item) == null ? 0 : inventory.get(item)) + amount);
    }

    public void remove(Item item, int amount) {
        if (item == null) return;
        if (item.equals(Item.pieces_d_or)) money = Math.max(money - amount, 0);
        else {
            if (inventory.get(item) == null) return;
            if (inventory.get(item) <= amount) inventory.remove(item);
            else inventory.put(item, inventory.get(item) - amount);
        }
    }

    public boolean has(Item item, int amount) {
        if (item == null) return false;
        if (item.equals(Item.pieces_d_or)) return amount <= money;
        else return inventory.get(item) != null && inventory.get(item) >= amount;
    }

    public int getHealth() {
        return health;
    }

    public boolean isDead() {
        return health <= 0;
    }

    public void takeDamage(int damage) {
        this.health -= damage;
    }

    public int getStrength() {
        return strength;
    }

    public int getDamage() {
        return strength + (weapon != null ? weapon.getDamage() : 0);
    }

    public int getAgility() {
        return agility;
    }

    public int getCharism() {
        return charism;
    }

    public Sex getSex() {
        return sex;
    }

    public int getMoney() {
        return money;
    }

    public void giveMoney(int amount) {
        money += amount;
    }

    public Item getWeapon() {
        return weapon;
    }

    public void setWeapon(Item weapon) {
        this.weapon = weapon;
    }

    public Item getProtection1() {
        return protection1;
    }

    public int getProtection() {
        return protection1 != null ? protection1.getProtection() : 0;
    }

    public void setProtection1(Item protection1) {
        this.protection1 = protection1;
    }

    public Item getTool() {
        return tool;
    }

    public void setTool(Item tool) {
        this.tool = tool;
    }

}
