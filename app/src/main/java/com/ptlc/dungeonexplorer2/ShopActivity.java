package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShopActivity extends Activity {

    private SharedPreferences parametres;
    private SharedPreferences achvmnts;

    private Button[] buttons = new Button[4];
    private TextView[] exchangesTextviews = new TextView[4];
    private Exchange[] exchanges = new Exchange[4];

    private Player player;

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.shop);
        initialize();
        Log.w("player JSON", "player : " + new Intent().getStringExtra("player"));
        player = new Gson().fromJson(new Intent().getStringExtra("player"), Player.class);
    }

    private void initialize() {
        ((TextView)findViewById(R.id.title)).setText(generateShopName());
        exchangesTextviews[0] = findViewById(R.id.echange1);
        buttons[0] = findViewById(R.id.button1);
        exchangesTextviews[1] = findViewById(R.id.echange2);
        buttons[1] = findViewById(R.id.button2);
        exchangesTextviews[2] = findViewById(R.id.echange3);
        buttons[2] = findViewById(R.id.button3);
        exchangesTextviews[3] = findViewById(R.id.echange4);
        buttons[3] = findViewById(R.id.button4);
        achvmnts = getSharedPreferences("succès.pkm", 0);
        parametres = getSharedPreferences("parametres.saucisse", 0);
        for (int i = 0; i < 4; i++) {
            int rdm = getRandom(1, 15);
            if (rdm == 1) _set_exchange(i, new Exchange(Item.boite_de_cookies,1,new Pair(Item.pieces_d_or, getRandom(8, 10))));
            if (rdm == 2) _set_exchange(i, new Exchange(Item.oreiller, 1, new Pair(Item.plume, getRandom(4, 7))));
            if (rdm == 3) _set_exchange(i, new Exchange(Item.plume, getRandom(4, 7), new Pair(Item.oreiller, 1)));
            if (rdm == 4) _set_exchange(i, new Exchange(Item.fiole_verte, 1, new Pair(Item.fiole_vide, 1), new Pair(Item.poudre_verte, getRandom(1, 2))));
            if (rdm == 5) _set_exchange(i, new Exchange(Item.baton_magique_bleu, 1, new Pair(Item.baton, getRandom(1, 2)), new Pair(Item.amulette_bleue, 1), new Pair(Item.pieces_d_or, getRandom(6, 8))));
            if (rdm == 6) _set_exchange(i, new Exchange(Item.pieces_d_or, getRandom(4, 12), new Pair(Item.casque_en_amethyste, 1)));
            if (rdm == 7) _set_exchange(i, new Exchange(Item.pieces_d_or, getRandom(2, 3), new Pair(Item.plume, getRandom(2, 3))));
            if (rdm == 8) _set_exchange(i, new Exchange(Item.fiole_jaune, 1, new Pair(Item.fiole_rouge, 1), new Pair(Item.poudre_verte, getRandom(1, 2)), new Pair(Item.pieces_d_or, getRandom(1, 3))));
            if (rdm == 9) _set_exchange(i, new Exchange(Item.amulette_verte, 1, new Pair(Item.fiole_jaune, 1), new Pair(Item.amulette_bleue, 1)));
            if (rdm == 10) _set_exchange(i, new Exchange(Item.viande_cuite, 1, new Pair(Item.viande_crue, 1), new Pair(Item.pieces_d_or, getRandom(1, 2))));
            if (rdm == 11) {
                int tmp = getRandom(1, 4);
                _set_exchange(i, new Exchange(Item.viande_cuite, tmp-1, new Pair(Item.viande_crue, tmp)));
            }
            if (rdm == 12) _set_exchange(i, new Exchange(Item.epee_elfique_1, 1, new Pair(Item.baton, getRandom(1, 2)), new Pair(Item.pieces_d_or, getRandom(2, 4))));
            if (rdm == 13) _set_exchange(i, new Exchange(Item.pieces_d_or, getRandom(22, 34), new Pair(Item.rubis, 1), new Pair(Item.prisme_noir, 1), new Pair(Item.diamant, 1)));
            if (rdm == 14) _set_exchange(i, new Exchange(Item.baton_magique_rouge, 1, new Pair(Item.barre_de_fer, 1), new Pair(Item.saphir, 1)));
            if (rdm == 15) _set_exchange(i, new Exchange(Item.baton_magique_vert, 1, new Pair(Item.baton, 1), new Pair(Item.amulette_verte, 1), new Pair(Item.pieces_d_or, getRandom(1, 5))));
        }
        buttons[0].setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                _start_exchange(exchanges[0]);
            }
        });
        buttons[1].setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                _start_exchange(exchanges[1]);
            }
        });
        buttons[2].setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                _start_exchange(exchanges[2]);
            }
        });
        buttons[3].setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                _start_exchange(exchanges[3]);
            }
        });
    }

    private String generateShopName() {
        String shopName = getRandom(1, 2) == 1 ? "La boutique " : "Le magasin ";
        int rdm = getRandom(1, 4);
        if (rdm == 1) {
            rdm = getRandom(1, 3);
            if (rdm == 1) shopName += "du vieux ";
            if (rdm == 2) shopName += "du bon ";
            if (rdm == 3) shopName += "du mauvais ";
            rdm = getRandom(1, 6);
            if (rdm == 1) shopName += "poulet";
            if (rdm == 2) shopName += "chasseur du dimanche";
            if (rdm == 3) shopName += "goélan";
            if (rdm == 4) shopName += "gnôme";
            if (rdm == 5) shopName += "chaudron";
            if (rdm == 6) shopName += "chaton";
        } else if (rdm == 2) {
            rdm = getRandom(1, 3);
            if (rdm == 1) shopName += "de la vieille ";
            if (rdm == 2) shopName += "de la chevrognue ";
            if (rdm == 3) shopName += "de la bonne ";
            rdm = getRandom(1, 3);
            if (rdm == 1) shopName += "mouette";
            if (rdm == 2) shopName += "saucisse";
            if (rdm == 3) shopName += "magie";
        } else if (rdm == 3) {
            rdm = getRandom(1, 3);
            if (rdm == 1) shopName += "du p�cheur ";
            if (rdm == 2) shopName += "du cookie ";
            if (rdm == 3) shopName += "du cochon ";
            rdm = getRandom(1, 3);
            if (rdm == 1) shopName += "agit�";
            if (rdm == 2) shopName += "badass";
            if (rdm == 3) shopName += "violet";
        } else if (rdm == 4) {
            rdm = getRandom(1, 6);
            if (rdm == 1) shopName += "de la cr�pe ";
            if (rdm == 2) shopName += "de la mouette ";
            if (rdm == 3) shopName += "de l'assiette ";
            if (rdm == 4) shopName += "de l'asperge ";
            if (rdm == 5) shopName += "de l'araign�e ";
            if (rdm == 6) shopName += "de la pierre ";
            rdm = getRandom(1, 3);
            if (rdm == 1) shopName += "volante";
            if (rdm == 2) shopName += "bleue";
            if (rdm == 3) shopName += "chauve";
        }
        return shopName;
    }

    private void _search_achievement(Exchange exchange) {
        if (Item.rubis.equals(exchange.getItem(0)) && Item.prisme_noir.equals(exchange.getItem(1)) && Item.diamant.equals(exchange.getItem(2)) && Item.pieces_d_or.equals(exchange.getResult()) && !achvmnts.getString("revendeur_pierres", "").equals("1")) {
            achvmnts.edit().putString("revendeur_pierres", "1").commit();
            showMessage("Revendeur de pierres précieuses   ?");
        }
    }

    private void _set_exchange(int n, Exchange exchange) {
        exchanges[n] = exchange;
        exchangesTextviews[n].setText(exchange.toString());
        // TODO : if (parametres.getString("mojis", "").equals("0"))
    }

    private void _start_exchange(Exchange exchange) {
        if (exchange.make(player)) {
            showMessage("Échange " + exchange.toString() + " effectué");
            _search_achievement(exchange);
        } else {
            showMessage("Vous ne possédez pas tous les éléments nécessaires");
        }
    }

    private int getRandom(int paramInt1, int paramInt2) { return (new Random()).nextInt(paramInt2 - paramInt1 + 1) + paramInt1; }

    private void showMessage(String paramString) { Toast.makeText(getApplicationContext(), paramString, Toast.LENGTH_SHORT).show(); }

    /*public ArrayList<Double> getCheckedItemPositionsToArray(ListView paramListView) {
        ArrayList arrayList = new ArrayList();
        SparseBooleanArray sparseBooleanArray = paramListView.getCheckedItemPositions();
        for (byte b = 0;; b++) {
            if (b >= sparseBooleanArray.size())
                return arrayList;
            if (sparseBooleanArray.valueAt(b))
                arrayList.add(Double.valueOf(sparseBooleanArray.keyAt(b)));
        }
    }*/

}
