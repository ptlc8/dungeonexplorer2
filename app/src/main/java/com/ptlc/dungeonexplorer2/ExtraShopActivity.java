package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class ExtraShopActivity extends Activity {
    private double arme_price = 0.0D;

    private TextView base_arme;

    private TextView basics_points;

    private Button button_base_arme;

    private Button button_basics_points;

    private SharedPreferences highscore;

    private HorizontalScrollView hscroll1;

    private LinearLayout linear1;

    private LinearLayout linear3;

    private LinearLayout linear4;

    private LinearLayout linear5;

    private LinearLayout linear6;

    private LinearLayout linear7;

    private TextView money;

    private SharedPreferences stats_bonus;

    private TextView text_base_ame;

    private TextView text_basics_points;

    private TextView title;

    private ScrollView vscroll1;

    private void _actualiser() {
        this.base_arme.setText("am�liorer (-�?)");
        this.button_basics_points.setText("+? (-�?)");
        this.money.setText(this.highscore.getString("TotalCoins", "").concat(" ?"));
        if (this.stats_bonus.getString("base_arme", "").equals("rien")) {
            this.button_base_arme.setText(this.button_base_arme.getText().toString().replace("�", "20"));
            this.arme_price = 20.0D;
        }
        if (this.stats_bonus.getString("base_arme", "").equals("B�ton")) {
            this.button_base_arme.setText(this.button_base_arme.getText().toString().replace("�", "60"));
            this.arme_price = 60.0D;
            this.base_arme.setText(this.base_arme.getText().toString().replace("aucune", this.stats_bonus.getString("base_arme", "")));
        }
        if (this.stats_bonus.getString("base_arme", "").equals("Couteau")) {
            this.button_base_arme.setText(this.button_base_arme.getText().toString().replace("�", "122"));
            this.arme_price = 122.0D;
            this.base_arme.setText(this.base_arme.getText().toString().replace("aucune", this.stats_bonus.getString("base_arme", "")));
        }
        if (this.stats_bonus.getString("base_arme", "").equals("Machette")) {
            this.button_base_arme.setText("Niveau max.");
            this.base_arme.setText(this.base_arme.getText().toString().replace("aucune", this.stats_bonus.getString("base_arme", "")));
            this.button_base_arme.setEnabled(false);
        }
        this.basics_points.setText(String.valueOf((long)(30.0D + Double.parseDouble(this.stats_bonus.getString("sup_points", "")))));
        this.button_basics_points.setText(this.button_basics_points.getText().toString().replace("�", String.valueOf((long)((Double.parseDouble(this.stats_bonus.getString("sup_points", "")) + 1.0D) * 10.0D))));
    }

    private int getRandom(int paramInt1, int paramInt2) { return (new Random()).nextInt(paramInt2 - paramInt1 + 1) + paramInt1; }

    private void initialize() {
        this.linear1 = (LinearLayout)findViewById(R.id.linear1);
        this.vscroll1 = (ScrollView)findViewById(R.id.vscroll1);
        this.title = (TextView)findViewById(R.id.title);
        this.hscroll1 = (HorizontalScrollView)findViewById(R.id.hscroll1);
        this.linear6 = (LinearLayout)findViewById(R.id.linear6);
        this.money = (TextView)findViewById(R.id.money);
        this.linear3 = (LinearLayout)findViewById(R.id.linear3);
        this.linear7 = (LinearLayout)findViewById(R.id.linear7);
        this.linear4 = (LinearLayout)findViewById(R.id.linear4);
        this.linear5 = (LinearLayout)findViewById(R.id.linear5);
        this.text_base_ame = (TextView)findViewById(R.id.text_base_ame);
        this.base_arme = (TextView)findViewById(R.id.base_arme);
        this.button_base_arme = (Button)findViewById(R.id.button_base_arme);
        this.text_basics_points = (TextView)findViewById(R.id.text_basics_points);
        this.basics_points = (TextView)findViewById(R.id.basics_points);
        this.button_basics_points = (Button)findViewById(R.id.button_basics_points);
        this.stats_bonus = getSharedPreferences("stats.txt", 0);
        this.highscore = getSharedPreferences("LaCr�meDeLaCr�me", 0);
        this.button_base_arme.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                if (Double.parseDouble(ExtraShopActivity.this.highscore.getString("TotalCoins", "")) - ExtraShopActivity.this.arme_price > -1.0D) {
                    ExtraShopActivity.this.highscore.edit().putString("TotalCoins", String.valueOf((long)(Double.parseDouble(ExtraShopActivity.this.highscore.getString("TotalCoins", "")) - ExtraShopActivity.this.arme_price))).commit();
                    if (ExtraShopActivity.this.stats_bonus.getString("base_arme", "").equals("rien")) {
                        ExtraShopActivity.this.stats_bonus.edit().putString("base_arme", "B�ton").commit();
                    } else if (ExtraShopActivity.this.stats_bonus.getString("base_arme", "").equals("B�ton")) {
                        ExtraShopActivity.this.stats_bonus.edit().putString("base_arme", "Couteau").commit();
                    } else if (ExtraShopActivity.this.stats_bonus.getString("base_arme", "").equals("Couteau")) {
                        ExtraShopActivity.this.stats_bonus.edit().putString("base_arme", "Machette").commit();
                    }
                } else {
                    ExtraShopActivity.this.showMessage("Vous n'avez pas assez de billets volants");
                }
                ExtraShopActivity.this._actualiser();
            }
        });
        this.button_basics_points.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                if (Double.parseDouble(ExtraShopActivity.this.highscore.getString("TotalCoins", "")) - (Double.parseDouble(ExtraShopActivity.this.stats_bonus.getString("sup_points", "")) + 1.0D) * 10.0D > -1.0D) {
                    ExtraShopActivity.this.highscore.edit().putString("TotalCoins", String.valueOf((long)(Double.parseDouble(ExtraShopActivity.this.highscore.getString("TotalCoins", "")) - (Double.parseDouble(ExtraShopActivity.this.stats_bonus.getString("sup_points", "")) + 1.0D) * 10.0D))).commit();
                    ExtraShopActivity.this.stats_bonus.edit().putString("sup_points", String.valueOf((long)(Double.parseDouble(ExtraShopActivity.this.stats_bonus.getString("sup_points", "")) + 1.0D))).commit();
                } else {
                    ExtraShopActivity.this.showMessage("Vous n'avez pas assez de billets volants");
                }
                ExtraShopActivity.this._actualiser();
            }
        });
    }

    private void initializeLogic() { _actualiser(); }

    private void showMessage(String paramString) { Toast.makeText(getApplicationContext(), paramString, 0).show(); }

    public ArrayList<Double> getCheckedItemPositionsToArray(ListView paramListView) {
        ArrayList arrayList = new ArrayList();
        SparseBooleanArray sparseBooleanArray = paramListView.getCheckedItemPositions();
        for (byte b = 0;; b++) {
            if (b >= sparseBooleanArray.size())
                return arrayList;
            if (sparseBooleanArray.valueAt(b))
                arrayList.add(Double.valueOf(sparseBooleanArray.keyAt(b)));
        }
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.extra_shop);
        initialize();
        initializeLogic();
    }
}
