package com.ptlc.dungeonexplorer2;

import com.ptlc.dungeonexplorer2.render.Card;
import com.ptlc.dungeonexplorer2.rooms.Chests;
import com.ptlc.dungeonexplorer2.rooms.Corridor;
import com.ptlc.dungeonexplorer2.rooms.DarkRoom;
import com.ptlc.dungeonexplorer2.rooms.Room;
import com.ptlc.dungeonexplorer2.rooms.Shop;
import com.ptlc.dungeonexplorer2.rooms.Stairs;

import java.util.Random;

public abstract class Game {

    private int state;
    private int score = 0;
    private Player player;
    private Room room;

    private int vieux_fou = 0;

    //private String title, description;
    //private Card leftCard, rightCard;

    public Game() {
        player = new Player(10, 5, 5, 5, Player.Sex.M, /*Item.get(ChangeStats.getString(
                "base_arme", ""))*/Item.baton);
        //title = "Voulez-vous entrez dans le donjon ?";
        //description = "";
        //leftCard = Card.Yes;
        //rightCard = Card.No;
        state = State.START;
    }

    public void leftCard() {
        switch (state) {
            case State.START:
                next_room();
                state = State.ALIVE;
                break;
            case State.DEAD:
                onFinish();
                break;
            default:
                room.onAction1();
                if (player.isDead()) {
                    state = State.DEAD;
                } else if (room.nextRoom()) {
                    next_room();
                }
        }
    }

    public void rightCard() {
        switch (state) {
            case State.START:
            case State.DEAD:
                onFinish();
                break;
            default:
                room.onAction2();
                if (player.isDead()) {
                    state = State.DEAD;
                } else if (room.nextRoom()) {
                    next_room();
                }
        }
    }

    protected abstract void onFinish();

    private void next_room() {
        this.score += 1;
        int rdmNextRoom = 0;
        while (true) {
            if (state != State.START) rdmNextRoom = getRandom(1, 17);
            if ((rdmNextRoom < 5) && !(room instanceof Corridor)) { // 5
                room = new Corridor();
                return;
            } else if (rdmNextRoom < 13) { // 8
                room = new DarkRoom(player, score);
                return;
            } else if ((rdmNextRoom < 15) && !(room instanceof Shop)) { // 2
                //room = new Shop(getApplicationContext(), player); TODO
                return;
            } else if ((rdmNextRoom < 17) && !(room instanceof Stairs)) { // 2
                room = new Stairs();
                return;
            } else if (rdmNextRoom < 18 && !(room instanceof Chests) && this.score > 20) { // 1
                room = new Chests(player, score);
                return;
            }
        }
    }

    private static int getRandom(int minValue, int maxValue) {
        return new Random().nextInt((maxValue - minValue) + 1) + minValue;
    }

    public String getTitle() {
        if (state == State.START) return "L'entrée";
        if (state == State.DEAD) return "Vous êtes mort";
        return room.getTitle() + " (" + score + ")";
    }

    public String getDescription() {
        if (state == State.START) return "Voulez-vous entrez dans le donjon ?";
        if (state == State.DEAD) return room.getDescription() + " \nVous avez fait un score de " + score + " et vous avez gagné " + player.getMoney() + " 💰. ";
        return room.getDescription();
    }

    public Card getLeftCard() {
        if (state == State.START) return Card.Yes;
        if (state == State.DEAD) return Card.Death1;
        return room.getLeftCard();
    }

    public Card getRightCard() {
        if (state == State.START) return Card.No;
        if (state == State.DEAD) return Card.Death2;
        return room.getRightCard();
    }

    public int getScore() {
        return score;
    }

    public Player getPlayer() {
        return player;
    }

    void setState(int state) { // only for Player ; tmp ?
        this.state = state;
    }

}
