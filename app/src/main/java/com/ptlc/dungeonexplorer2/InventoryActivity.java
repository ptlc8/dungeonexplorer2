package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class InventoryActivity extends Activity {
    private double CurrentSelected = 0.0D;

    private String Selected = "";

    private ArrayList<String> all_items = new ArrayList();

    private TextView argent;

    private TextView description;

    private SharedPreferences inventory;

    private ArrayList<String> inventory_editor = new ArrayList();

    private ArrayList<String> inventory_id = new ArrayList();

    private LinearLayout linear1;

    private LinearLayout linear2;

    private SharedPreferences parametres;

    private Spinner select_inventaire;

    private Button supprimer;

    private TextView title;

    private double tmp = 0.0D;

    private Button utiliser;

    private void _actualiser_inventory() {
        this.inventory_editor.clear();
        this.all_items.clear();
        this.inventory_id.clear();
        this.all_items.add("Viande crue");
        this.all_items.add("Plume");
        this.all_items.add("Cl�");
        this.all_items.add("Fiole rouge");
        this.all_items.add("Fiole vide");
        this.all_items.add("Poudre verte");
        this.all_items.add("Casque en am�thyste");
        this.all_items.add("B�ton");
        this.all_items.add("Couteau");
        this.all_items.add("Amulette bleue");
        this.all_items.add("TalkieWalkie");
        this.all_items.add("Viande cuite");
        this.all_items.add("Bo�te de cookies");
        this.all_items.add("B�ton magique bleu");
        this.all_items.add("Oreiller");
        this.all_items.add("Fiole verte");
        this.all_items.add("Fiole jaune");
        this.all_items.add("Amulette verte");
        this.all_items.add("Masse");
        this.all_items.add("Poudre lumineuse");
        this.all_items.add("Prisme noir");
        this.all_items.add("Rubis");
        this.all_items.add("�p�e elfique ?");
        this.all_items.add("Diamant");
        this.all_items.add("Barre de fer");
        this.all_items.add("Machette");
        this.all_items.add("Saphir");
        this.all_items.add("Eau b�nite");
        this.all_items.add("B�ton magique vert");
        this.all_items.add("B�ton magique rouge");
        this.all_items.add("Fiole lumineuse verte");
        while (true) {
            if (Double.parseDouble(this.inventory.getString((String)this.all_items.get(0), "")) > 0.0D) {
                if (this.inventory.getString("arme", "").equals(this.all_items.get(0))) {
                    this.inventory_editor.add(((String)this.all_items.get(0)).concat(" (�").concat(this.inventory.getString((String)this.all_items.get(0), "").concat(") - �quip�")));
                } else if (this.inventory.getString("protection1", "").equals(this.all_items.get(0))) {
                    this.inventory_editor.add(((String)this.all_items.get(0)).concat(" (�").concat(this.inventory.getString((String)this.all_items.get(0), "").concat(") - port�")));
                } else if (this.inventory.getString("outil", "").equals(this.all_items.get(0))) {
                    this.inventory_editor.add(((String)this.all_items.get(0)).concat(" (�").concat(this.inventory.getString((String)this.all_items.get(0), "").concat(") - utilis�")));
                } else {
                    this.inventory_editor.add(((String)this.all_items.get(0)).concat(" (�").concat(this.inventory.getString((String)this.all_items.get(0), "").concat(")")));
                }
                this.inventory_id.add((String)this.all_items.get(0));
            }
            if (this.all_items.size() == 1) {
                if (this.inventory_editor.size() == 0)
                    this.inventory_editor.add("Votre inventaire est vide");
                this.select_inventaire.setAdapter(new ArrayAdapter(getBaseContext(), 17367049, this.inventory_editor));
                ((ArrayAdapter)this.select_inventaire.getAdapter()).notifyDataSetChanged();
                this.select_inventaire.setSelection((int)this.CurrentSelected);
                return;
            }
            this.all_items.remove(0);
        }
    }

    private int getRandom(int paramInt1, int paramInt2) { return (new Random()).nextInt(paramInt2 - paramInt1 + 1) + paramInt1; }

    private void initialize() {
        this.linear2 = (LinearLayout)findViewById(R.id.linear2);
        this.select_inventaire = (Spinner)findViewById(R.id.select_inventaire);
        this.description = (TextView)findViewById(R.id.description);
        this.linear1 = (LinearLayout)findViewById(R.id.linear1);
        this.title = (TextView)findViewById(R.id.title);
        this.argent = (TextView)findViewById(R.id.argent);
        this.utiliser = (Button)findViewById(R.id.utiliser);
        this.supprimer = (Button)findViewById(R.id.supprimer);
        this.inventory = getSharedPreferences("inventaire.xD", 0);
        this.parametres = getSharedPreferences("parametres.saucisse", 0);
        this.utiliser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                if (InventoryActivity.this.Selected.equals("Viande crue")) {
                    InventoryActivity.this.tmp = InventoryActivity.this.getRandom(-1, 2);
                    InventoryActivity.this.inventory.edit().putString("Vie", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Vie", "")) + InventoryActivity.this.tmp))).commit();
                    InventoryActivity.this.showMessage("Vie : +".concat(String.valueOf((long)InventoryActivity.this.tmp).concat("?")));
                    InventoryActivity.this.inventory.edit().putString("Viande crue", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Viande crue", "")) - 1.0D))).commit();
                } else if (InventoryActivity.this.Selected.equals("Viande cuite")) {
                    InventoryActivity.this.tmp = InventoryActivity.this.getRandom(1, 4);
                    InventoryActivity.this.inventory.edit().putString("Vie", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Vie", "")) + InventoryActivity.this.tmp))).commit();
                    InventoryActivity.this.showMessage("Vie : +".concat(String.valueOf((long)InventoryActivity.this.tmp).concat("?")));
                    InventoryActivity.this.inventory.edit().putString("Viande cuite", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Viande cuite", "")) - 1.0D))).commit();
                } else if (InventoryActivity.this.Selected.equals("Bo�te de cookies")) {
                    InventoryActivity.this.inventory.edit().putString("Vie", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Vie", "")) + 9.0D))).commit();
                    InventoryActivity.this.showMessage("Vie : +".concat(String.valueOf(9L).concat("?")));
                    InventoryActivity.this.inventory.edit().putString("Bo�te de cookies", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Bo�te de cookies", "")) - 1.0D))).commit();
                } else if (InventoryActivity.this.Selected.equals("B�ton")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "B�ton").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "1").commit();
                } else if (InventoryActivity.this.Selected.equals("Couteau")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "Couteau").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "2").commit();
                } else if (InventoryActivity.this.Selected.equals("B�ton magique bleu")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "B�ton magique bleu").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "2~8").commit();
                } else if (InventoryActivity.this.Selected.equals("Masse")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "Masse").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "4").commit();
                } else if (InventoryActivity.this.Selected.equals("�p�e elfique ?")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "�p�e elfique ?").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "3~5").commit();
                } else if (InventoryActivity.this.Selected.equals("Casque en am�thyste")) {
                    InventoryActivity.this.inventory.edit().putString("protection1", "Casque en am�thyste").commit();
                    InventoryActivity.this.inventory.edit().putString("abso_protection1", "2").commit();
                } else if (InventoryActivity.this.Selected.equals("�quip�")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "0").commit();
                } else if (InventoryActivity.this.Selected.equals("port�1")) {
                    InventoryActivity.this.inventory.edit().putString("protection1", "").commit();
                    InventoryActivity.this.inventory.edit().putString("abso_protection1", "0").commit();
                } else if (InventoryActivity.this.Selected.equals("Fiole rouge")) {
                    InventoryActivity.this.inventory.edit().putString("Fiole rouge", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Fiole rouge", "")) - 1.0D))).commit();
                    InventoryActivity.this.showMessage("?????");
                    InventoryActivity.this.inventory.edit().putString("Fiole vide", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Fiole vide", "")) + 1.0D))).commit();
                } else if (InventoryActivity.this.Selected.equals("Fiole verte")) {
                    InventoryActivity.this.inventory.edit().putString("Fiole verte", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Fiole verte", "")) - 1.0D))).commit();
                    InventoryActivity.this.showMessage("?????");
                    InventoryActivity.this.inventory.edit().putString("Fiole vide", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Fiole vide", "")) + 1.0D))).commit();
                } else if (InventoryActivity.this.Selected.equals("Fiole jaune")) {
                    InventoryActivity.this.inventory.edit().putString("Fiole jaune", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Fiole jaune", "")) - 1.0D))).commit();
                    InventoryActivity.this.showMessage("?????");
                    InventoryActivity.this.inventory.edit().putString("Fiole vide", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Fiole vide", "")) + 1.0D))).commit();
                } else if (InventoryActivity.this.Selected.equals("Barre de fer")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "Barre de fer").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "2").commit();
                } else if (InventoryActivity.this.Selected.equals("Machette")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "Machette").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "3").commit();
                } else if (InventoryActivity.this.Selected.equals("Eau b�nite")) {
                    InventoryActivity.this.inventory.edit().putString("Vie", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Vie", "")) + 11.0D))).commit();
                    InventoryActivity.this.showMessage("Vie : +".concat(String.valueOf(11L).concat("?")));
                    InventoryActivity.this.inventory.edit().putString("Eau b�nite", String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString("Eau b�nite", "")) - 1.0D))).commit();
                } else if (InventoryActivity.this.Selected.equals("B�ton magique vert")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "B�ton magique vert").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "3~9").commit();
                } else if (InventoryActivity.this.Selected.equals("B�ton magique rouge")) {
                    InventoryActivity.this.inventory.edit().putString("arme", "B�ton magique rouge").commit();
                    InventoryActivity.this.inventory.edit().putString("degats_arme", "0~9").commit();
                } else if (InventoryActivity.this.Selected.equals("Fiole lumineuse verte")) {
                    InventoryActivity.this.inventory.edit().putString("outil", "Fiole lumineuse verte").commit();
                } else if (InventoryActivity.this.Selected.equals("utilis�")) {
                    InventoryActivity.this.inventory.edit().putString("outil", "").commit();
                } else {
                    InventoryActivity.this.showMessage("Erreur <?>");
                }
                InventoryActivity.this.CurrentSelected = InventoryActivity.this.select_inventaire.getSelectedItemPosition();
                InventoryActivity.this._actualiser_inventory();
            }
        });
        this.supprimer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View param1View) {
                InventoryActivity.this.inventory.edit().putString((String)InventoryActivity.this.inventory_id.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition()), String.valueOf((long)(Double.parseDouble(InventoryActivity.this.inventory.getString((String)InventoryActivity.this.inventory_id.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition()), "")) - 1.0D))).commit();
                InventoryActivity.this.CurrentSelected = InventoryActivity.this.select_inventaire.getSelectedItemPosition();
                InventoryActivity.this._actualiser_inventory();
            }
        });
        this.select_inventaire.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView param1AdapterView, View param1View, int param1Int, long param1Long) {
                if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("est vide")) {
                    InventoryActivity.this.utiliser.setEnabled(false);
                    InventoryActivity.this.utiliser.setText("Utiliser");
                    InventoryActivity.this.supprimer.setEnabled(false);
                    InventoryActivity.this.description.setText("Trouver des objets en tuant, fouillant des ennemis ou en trouvant des coffres, vous pouvez aussi en �changer dans les boutiques du donjon.");
                    InventoryActivity.this.Selected = "vide";
                } else {
                    InventoryActivity.this.supprimer.setEnabled(true);
                    if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Viande crue")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Consommer");
                        InventoryActivity.this.description.setText("Permet de r�cup�rer de la vie; il est vivement conseill� de la faire cuire avant de consommer.\n   -1? ~ +2?");
                        InventoryActivity.this.Selected = "Viande crue";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("B�ton") && !((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("B�ton magique")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("Une arme tr�s basique mais qui peut aussi servir � faire des armes plus puissantes...\n   +1?");
                        InventoryActivity.this.Selected = "B�ton";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Couteau")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("Cut ! Cut ! Cut !\n   +2?");
                        InventoryActivity.this.Selected = "Couteau";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Casque en am�thyste")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Porter");
                        InventoryActivity.this.description.setText("Fabriqu� d'une pierre peu commune, ce casque vous prot�gera la t�te de pas mal de coup de vos adversaires.\n   +2?");
                        InventoryActivity.this.Selected = "Casque en am�thyste";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Cl�")) {
                        InventoryActivity.this.utiliser.setEnabled(false);
                        InventoryActivity.this.utiliser.setText("Utiliser");
                        InventoryActivity.this.description.setText("Pour ouvrir un coffre ou une porte ferm�e � double tour, il vous la faudra : la cl�. Malgr� l'ing�niosit� des cr�ateurs de ce donjon, ils n'ont m�me pas pens� � faire des cl�s diff�rentes.");
                        InventoryActivity.this.Selected = "Cl�";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("TalkieWalkie")) {
                        InventoryActivity.this.utiliser.setEnabled(false);
                        InventoryActivity.this.utiliser.setText("Utiliser");
                        InventoryActivity.this.description.setText("L'objet le plus inattendu que vous pourrez trouver dans ce donjon.\n   Coming Soon ?");
                        InventoryActivity.this.Selected = "TalkieWalkie";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Viande cuite")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Consommer");
                        InventoryActivity.this.description.setText("Rien de tel qu'un bon beef-tch�que pour r�cup�rer de la vie.\n   +1? ~ +4?");
                        InventoryActivity.this.Selected = "Viande cuite";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("B�ton magique bleu")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("Un b�ton magique bleu, mais � quoi �a sert ? � faire de la magie, pardis ! Mais pourquoi bleu ? �a c'est � vous de le d�couvrir. Coming soon !\n   +2~8?");
                        InventoryActivity.this.Selected = "B�ton magique bleu";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Oreiller")) {
                        InventoryActivity.this.utiliser.setEnabled(false);
                        InventoryActivity.this.utiliser.setText("Utiliser");
                        InventoryActivity.this.description.setText("Un oreiller ? Dans un donjon ? Ou plut�t un sac de plumes, les oreillers n'ont qu'une seule utilit� : l'investissement dans le march� des plumes.");
                        InventoryActivity.this.Selected = "Oreiller";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Fiole rouge")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Boire");
                        InventoryActivity.this.description.setText("Mais qu'est-ce que c'est que �a ? Encore une magouille du cr�ateur ? En tout cas �a � pas l'air bon.");
                        InventoryActivity.this.Selected = "Fiole rouge";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Fiole verte")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Boire");
                        InventoryActivity.this.description.setText("Un liquide vert tr�s �pais, tel de la vase fra�chement ramass� lors de la derni�re �clipse sur un ancien champ de bataille.");
                        InventoryActivity.this.Selected = "Fiole verte";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Fiole jaune")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Boire");
                        InventoryActivity.this.description.setText("Encore un liquide bizarre ? Il y en a de toutes les couleurs pour tous les go�ts ! Et m�me une �tiquette pour en savoir les effets !?");
                        InventoryActivity.this.Selected = "Fiole jaune";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Bo�te de cookies")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Consommer");
                        InventoryActivity.this.description.setText("De bons cookies, comme ceux que l'on peut trouver � Intermarch�, miam...\n   +9?");
                        InventoryActivity.this.Selected = "Bo�te de cookies";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Masse")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("L'arme de bourrin par excellence.\n   +4?");
                        InventoryActivity.this.Selected = "Masse";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("�p�e elfique ?")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("???? ??????? ?.\n   +3~5?");
                        InventoryActivity.this.Selected = "�p�e elfique ?";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Prisme noir")) {
                        InventoryActivity.this.utiliser.setEnabled(false);
                        InventoryActivity.this.utiliser.setText("Utiliser");
                        InventoryActivity.this.description.setText("Certains l'appeleront �Obsidienne�, voire �Prisme noir�, mais NOUS nous l'appelerons �?????? ????� ! Quoique... c'est peut-�tre un peu compliqu�, non ?");
                        InventoryActivity.this.Selected = "Prisme noir";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Barre de fer")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("Aucune description pour cet objet.\n   +2?");
                        InventoryActivity.this.Selected = "Barre de fer";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Machette")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("Aucune description pour cet objet.\n   +3?");
                        InventoryActivity.this.Selected = "Machette";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Eau b�nite")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Boire");
                        InventoryActivity.this.description.setText("L'eau b�nite, r�sout tout vos probl�mes. Attention l'abus d'eau b�nite est �hic� pour la sant�.\n   +11?");
                        InventoryActivity.this.Selected = "Eau b�nite";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("B�ton magique vert")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("Encore un b�ton magique ! Mais cette fois il est plus puissant ! C'est pour �a qu'il a �t� compliqu� � aqu�rir ? Bien s�r que oui : plus c'est puissant, plus c'est rare, plus c'est cher.\n   +3~9?");
                        InventoryActivity.this.Selected = "B�ton magique vert";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("B�ton magique rouge")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("�quiper");
                        InventoryActivity.this.description.setText("On raconte que les b�tons magiques rouges ont �t� con�u par une communaut� de mage tr�s ferm�e, les seuls personnes qui ont tent� de les trahir ont �t� retrouv�s ivres-mortes. C'est pour �a que �a sent l'alcool ?\n   +0~9?");
                        InventoryActivity.this.Selected = "B�ton magique rouge";
                    } else if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("Fiole lumineuse verte")) {
                        InventoryActivity.this.utiliser.setEnabled(true);
                        InventoryActivity.this.utiliser.setText("Utiliser");
                        InventoryActivity.this.description.setText("Whaaa... c'est tout vert fluorescent et �a fait de la lumi�re; tr�s int�ressant pour y voir plus clair dans toutes ces sombres salles.\n   +20% de chance de trouver quelque-chose lors d'une inspection");
                        InventoryActivity.this.Selected = "Fiole lumineuse verte";
                    } else {
                        InventoryActivity.this.utiliser.setEnabled(false);
                        InventoryActivity.this.utiliser.setText("Utiliser");
                        InventoryActivity.this.description.setText("Aucune description pour cet objet.");
                        InventoryActivity.this.Selected = "autre";
                    }
                }
                if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("�quip�")) {
                    InventoryActivity.this.utiliser.setEnabled(true);
                    InventoryActivity.this.utiliser.setText("D�s�quiper");
                    InventoryActivity.this.Selected = "�quip�";
                }
                if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("port�")) {
                    InventoryActivity.this.utiliser.setEnabled(true);
                    InventoryActivity.this.utiliser.setText("Enlever");
                    InventoryActivity.this.Selected = "port�1";
                }
                if (((String)InventoryActivity.this.inventory_editor.get(InventoryActivity.this.select_inventaire.getSelectedItemPosition())).contains("utilis�")) {
                    InventoryActivity.this.utiliser.setEnabled(true);
                    InventoryActivity.this.utiliser.setText("Ranger");
                    InventoryActivity.this.Selected = "utilis�";
                }
            }

            public void onNothingSelected(AdapterView param1AdapterView) {}
        });
    }

    private void initializeLogic() {
        if (this.parametres.getString("mojis", "").equals("1")) {
            this.argent.setText(this.inventory.getString("?", "").concat(" ?"));
        } else {
            this.argent.setText(this.inventory.getString("?", "").concat(" $"));
        }
        _actualiser_inventory();
    }

    private void showMessage(String paramString) { Toast.makeText(getApplicationContext(), paramString, 0).show(); }

    public ArrayList<Double> getCheckedItemPositionsToArray(ListView paramListView) {
        ArrayList arrayList = new ArrayList();
        SparseBooleanArray sparseBooleanArray = paramListView.getCheckedItemPositions();
        for (byte b = 0;; b++) {
            if (b >= sparseBooleanArray.size())
                return arrayList;
            if (sparseBooleanArray.valueAt(b))
                arrayList.add(Double.valueOf(sparseBooleanArray.keyAt(b)));
        }
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.inventory);
        initialize();
        initializeLogic();
    }
}
