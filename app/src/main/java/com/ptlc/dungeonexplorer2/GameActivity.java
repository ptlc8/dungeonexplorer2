package com.ptlc.dungeonexplorer2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ptlc.dungeonexplorer2.rooms.Chests;
import com.ptlc.dungeonexplorer2.rooms.Corridor;
import com.ptlc.dungeonexplorer2.rooms.DarkRoom;
import com.ptlc.dungeonexplorer2.rooms.Room;
import com.ptlc.dungeonexplorer2.rooms.Shop;
import com.ptlc.dungeonexplorer2.rooms.Stairs;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends Activity {

    private SharedPreferences achievements;
    private SharedPreferences ChangeStats;
    private SharedPreferences HighScore;
    private SharedPreferences Inventaire_v2;
    private SharedPreferences parametres;

    private Intent intent = new Intent();

    private int state;
    private int score = 0;
    private Player player;
    private Room room;

    private TextView title;
    private TextView description;
    private ImageView button1;
    private ImageView button2;

    private TextView vieView;
    private TextView argentView;
    private TextView forceView;
    private TextView agiliteView;
    private TextView charismeView;
    private TextView sexeView;

    private ArrayList<String> all_items = new ArrayList<>();
    private Button modif_stats;
    private Button ouvrir_inventaire;
    private int vieux_fou = 0;

    @Override // valide
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        this.title = findViewById(R.id.title);
        this.description = findViewById(R.id.description);
        //this.inventaire = findViewById(R.id.inventaire);
        this.button1 = findViewById(R.id.image_button1);
        this.button2 = findViewById(R.id.image_button2);
        this.sexeView = findViewById(R.id.sex);
        this.vieView = findViewById(R.id.vie);
        this.agiliteView = findViewById(R.id.agilite);
        this.forceView = findViewById(R.id.force);
        this.charismeView = findViewById(R.id.charisme);
        this.argentView = findViewById(R.id.argent);
        this.ouvrir_inventaire = findViewById(R.id.ouvrir_inventaire);
        this.modif_stats = findViewById(R.id.modif_stats);
        this.Inventaire_v2 = getSharedPreferences("inventaire.xD", 0);
        this.HighScore = getSharedPreferences("LaCrèmeDeLaCrème", 0);
        this.ChangeStats = getSharedPreferences("stats.txt", 0);
        this.parametres = getSharedPreferences("parametres.saucisse", 0);
        this.achievements = getSharedPreferences("succès.pkm", 0);
        this.button1.setOnClickListener(new OnClickListener() {
            public void onClick(View _v) {
                onAction1();
            }
        });
        this.button2.setOnClickListener(new OnClickListener() {
            public void onClick(View _v) {
                onAction2();
            }
        });
        this.ouvrir_inventaire.setOnClickListener(new OnClickListener() {
            public void onClick(View _v) {
                intent.setClass(getApplicationContext(), InventoryActivity.class);
                startActivity(intent);
            }
        });
        this.modif_stats.setOnClickListener(new OnClickListener() {
            public void onClick(View _v) {
                intent.setClass(getApplicationContext(), SetStatsActivity.class);
                startActivity(intent);
            }
        });
        this.player = new Player(10, 5, 5, 5, Player.Sex.M, Item.get(ChangeStats.getString(
                "base_arme", "")));
        this.title.setText("Voulez-vous entrez dans le donjon ?");
        this.button1.setImageResource(R.drawable.yes_button);
        this.button2.setImageResource(R.drawable.no_button);
        state = State.START;
        this.ouvrir_inventaire.setEnabled(true);

        /*this.ChangeStats.edit().putString("Vie", String.valueOf((long) (15 + Integer.parseInt(this.ChangeStats.getString("sup_points", ""))))).apply();
        this.ChangeStats.edit().putString("force", "5").apply();
        this.ChangeStats.edit().putString("agilite", "5").apply();
        this.ChangeStats.edit().putString("charisme", "5").apply();
        this.ChangeStats.edit().putString("Sexe", String.valueOf((long) getRandom(1, 2))).apply();*/
        refresh();
    }

    @Override // valide
    public void onStart() {
        super.onStart();
        //refresh();
    }

    @Override // valide
    public void onBackPressed() {
        if (state == State.START) {
            finish();
            return;
        }
        AlertDialog.Builder exitDialog = new Builder(this);
        exitDialog.setTitle("Abandonner la partie ?");
        exitDialog.setMessage("Voulez-vous vraiment abandonner la partie ?");
        exitDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface _dialog, int _which) {
                finish();
            }
        });
        exitDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface _dialog, int _which) {
            }
        });
        exitDialog.create().show();
    }

    // valide
    private void refresh() {
        if (state == State.START) {
            /*this.Inventaire_v2.edit().putString("Vie", this.ChangeStats.getString("Vie", "")).apply();
            this.force = Integer.parseInt(this.ChangeStats.getString("force", ""));
            this.agilite = Integer.parseInt(this.ChangeStats.getString("agilite", ""));
            this.charisme = Integer.parseInt(this.ChangeStats.getString("charisme", ""));
            this.sexe = Integer.parseInt(this.ChangeStats.getString("Sexe", ""));*/
        }
        this.vieView.setText("Santé : " + player.getHealth() + " ❤");
        this.agiliteView.setText("Agilité :" + player.getAgility() + " 🏃");
        this.forceView.setText("Force : " + player.getStrength() + " 🏋");
        this.charismeView.setText("Charisme : " + player.getCharism() + " 🕶");
        this.argentView.setText("Argent : " + player.getMoney() + " 💰");
        if (player.getSex() == Player.Sex.F)
            this.sexeView.setText("♂");
        else
            this.sexeView.setText("♀");
        if (player.getWeapon() != null) {
            this.forceView.setText(this.forceView.getText().toString() + " + " + player.getWeapon().getDamage() + "💥");
        }
        if (player.getProtection1() != null) {
            this.vieView.setText(this.vieView.getText().toString() + " + " + player.getProtection1().getProtection() + "🛡");
        }
        if (this.parametres.getString("mojis", "1").equals("0")) {
            this.vieView.setText(this.vieView.getText().toString().replace("❤", "♥"));
            this.agiliteView.setText(this.agiliteView.getText().toString().replace("🏃", "A"));
            this.forceView.setText(this.forceView.getText().toString().replace("🏋", "F"));
            this.charismeView.setText(this.charismeView.getText().toString().replace("🕶", "C"));
            this.argentView.setText(this.argentView.getText().toString().replace("💰", "$"));
            this.vieView.setText(this.vieView.getText().toString().replace("🛡", "B"));
            this.forceView.setText(this.forceView.getText().toString().replace("💥", "🗡"));
        }
        _search_achvmnts();
    }

    // valide
    private void onAction1() {
        try { // TODO : à retirer plus tard
        if (room instanceof  DarkRoom) showMessage(((DarkRoom)room).state+"");
        if (state == State.START) {
            modif_stats.setEnabled(false);
            modif_stats.setBackgroundColor(0);
            ouvrir_inventaire.setEnabled(true);
            next_room();
            state = State.ALIVE;
        } else {
            room.onAction1();
            if (state == State.DEAD) {
                onDeath();
                return;
            } else if (room.nextRoom()) {
                next_room();
            }
        }
        title.setText(room.getTitle() + "(" + score + ")");
        description.setText(room.getDescription());
        button1.setImageBitmap(room.getLeftCard().getImage(getResources()));
        button2.setImageBitmap(room.getRightCard().getImage(getResources()));
        if (state == State.DEAD) onDeath();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), e.getCause().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // valide
    private void onAction2() {
        try { // TODO : à retirer plus tard
        if (room instanceof  DarkRoom) showMessage(((DarkRoom)room).state+"");
        if (state == State.START) {
            finish();
            return;
        } else {
            room.onAction2();
            if (state == State.DEAD) {
                onDeath();
                return;
            } else if (room.nextRoom()) {
                next_room();
            }
        }
        title.setText(room.getTitle() + "(" + score + ")");
        description.setText(room.getDescription());
        button1.setImageBitmap(room.getLeftCard().getImage(getResources()));
        button2.setImageBitmap(room.getRightCard().getImage(getResources()));
        if (state == State.DEAD) onDeath();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), e.getCause().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // valide
    private void onDeath() {
        title.setText("Vous êtes mort");
        button1.setImageResource(R.drawable.death1_button);
        button2.setImageResource(R.drawable.death2_button);
        description.setText(description.getText().toString() + " \nVous avez fait un score de " + score + " et vous avez gagné " + player.getMoney() + " 💰. ");
        if (Integer.parseInt(HighScore.getString("1s", "0")) < score) {
            HighScore.edit().putString("1s", String.valueOf(score)).apply();
            description.setText(description.getText().toString() + " Vous avez fait un nouveau reccord de score.");
        }
        if (Integer.parseInt(HighScore.getString("1a", "0")) < player.getMoney()) {
            HighScore.edit().putString("1a", Inventaire_v2.getString("💰", "")).apply();
            description.setText(description.getText().toString() + " Vous avez fait un nouveau reccord d'argentView.");
        }
        HighScore.edit().putString("TotalCoins", String.valueOf(Integer.parseInt(HighScore.getString("TotalCoins", "0")) + player.getMoney())).apply();
        ouvrir_inventaire.setEnabled(false);
        this.achievements.edit().putString("games", String.valueOf((Integer.parseInt(achievements.getString("games", "0")) + 1))).apply();
        if (achievements.getString("games", "0").equals("10"))
            showMessage("Apprenti   ✔");
        if (achievements.getString("games", "0").equals("50"))
            showMessage("Amateur   ✔");
        if (achievements.getString("games", "0").equals("100"))
            showMessage("Professionnel   ✔");
    }

    private void next_room() {
        this.score += 1;
        int rdmNextRoom = 0;
        while (true) {
            if (state != State.START) rdmNextRoom = getRandom(1, 17);
            if ((rdmNextRoom < 5) && !(room instanceof  Corridor)) { // 5
                room = new Corridor();
                return;
            } else if (rdmNextRoom < 13) { // 8
                room = new DarkRoom(player, score);
                return;
            } else if ((rdmNextRoom < 15) && !(room instanceof Shop)) { // 2
                room = new Shop(getApplicationContext(), player);
                return;
            } else if ((rdmNextRoom < 17) && !(room instanceof Stairs)) { // 2
                room = new Stairs();
                return;
            } else if (rdmNextRoom < 18 && !(room instanceof Chests) && this.score > 20) { // 1
                room = new Chests(player, score);
                return;
            }
        }
    }

    @Deprecated // compatible
    void _ajouter_a_inventaire(String _objet, int _nb) {
        int tmp;
        if (_objet.equals("Pièces d'or")) {
            tmp = getRandom(4, 8);
            player.giveMoney(tmp);
            showMessage("Argent : +" + String.valueOf((long) tmp) + " 💰");
            refresh();
        } else if (Item.get(_objet) != null) {
            player.give(Item.get(_objet), _nb);
        }
    }

    // valide
    private int getRandom(int minValue, int maxValue) {
        return new Random().nextInt((maxValue - minValue) + 1) + minValue;
    }

    // valide
    private void _search_achvmnts() {
        if (!achievements.getString("batons_magiques", "").equals("3")) {
            if (player.has(Item.baton_magique_bleu, 1) && !achievements.getString("baton_magique_bleu", "").equals("1")) {
                achievements.edit().putString("baton_magique_bleu", "1").apply();
                achievements.edit().putString("batons_magiques", String.valueOf(Integer.parseInt(this.achievements.getString("batons_magiques", "0")) + 1)).apply();
                showMessage("La magie des bâtons (" + achievements.getString("batons_magiques", "0") + "/3)");
            }
            if (player.has(Item.baton_magique_vert, 1) && !achievements.getString("baton_magique_vert", "").equals("1")) {
                achievements.edit().putString("baton_magique_vert", "1").apply();
                achievements.edit().putString("batons_magiques", String.valueOf(Integer.parseInt(achievements.getString("batons_magiques", "0")) + 1)).apply();
                showMessage("La magie des bâtons (" + achievements.getString("batons_magiques", "0") + "/3)");
            }
            if (player.has(Item.baton_magique_rouge, 1) && !achievements.getString("baton_magique_rouge", "").equals("1")) {
                achievements.edit().putString("baton_magique_rouge", "1").apply();
                achievements.edit().putString("batons_magiques", String.valueOf(Integer.parseInt(achievements.getString("batons_magiques", "0")) + 1)).apply();
                showMessage("La magie des bâtons (" + achievements.getString("batons_magiques", "") + "/3)");
            }
        }
        if (!achievements.getString("pierres", "").equals("4")) {
            if (player.has(Item.rubis, 1) && !achievements.getString("rubis", "").equals("1")) {
                achievements.edit().putString("rubis", "1").apply();
                achievements.edit().putString("pierres", String.valueOf(Integer.parseInt(achievements.getString("pierres", "0")) + 1)).apply();
                showMessage("Collectionneur de pierres précieuses (" + achievements.getString("pierres", "") + "/4)");
            }
            if (player.has(Item.diamant, 1) && !achievements.getString("diamant", "").equals("1")) {
                achievements.edit().putString("diamant", "1").apply();
                achievements.edit().putString("pierres", String.valueOf(Integer.parseInt(achievements.getString("pierres", "0")) + 1)).apply();
                showMessage("Collectionneur de pierres précieuses (" + achievements.getString("pierres", "") + "/4)");
            }
            if (player.has(Item.prisme_noir, 1) && !achievements.getString("prisme_noir", "").equals("1")) {
                achievements.edit().putString("prisme_noir", "1").apply();
                achievements.edit().putString("pierres", String.valueOf(Integer.parseInt(achievements.getString("pierres", "0")) + 1)).apply();
                showMessage("Collectionneur de pierres précieuses (" + achievements.getString("pierres", "") + "/4)");
            }
            if (player.has(Item.saphir, 1) && !achievements.getString("saphir", "").equals("1")) {
                achievements.edit().putString("saphir", "1").apply();
                achievements.edit().putString("pierres", String.valueOf(Integer.parseInt(achievements.getString("pierres", "0")) + 1)).apply();
                showMessage("Collectionneur de pierres précieuses (" + achievements.getString("pierres", "") + "/4)");
            }
        }
        if (!achievements.getString("vieux_fou", "").equals("1") && vieux_fou == 1) {
            this.achievements.edit().putString("vieux_fou", "1").apply();
            showMessage("Le vieux fou   ✔");
        }
        if (!this.achievements.getString("vieux_fou_2", "").equals("1") && this.vieux_fou == 2) {
            this.achievements.edit().putString("vieux_fou_2", "1").apply();
            showMessage("Le retour du vieux fou   ✔");
        }
    }

    // valide
    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    // valide
    public void setState(int state) {
        this.state = state;
    }

}
