package com.ptlc.dungeonexplorer2.render;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.HashMap;
import java.util.Map;

public class ImageLoader {

    private static Map<Integer, Bitmap> images = new HashMap<>();

    public static Bitmap loadBitmap(Resources res, int id) {
        if (images.get(id) != null) return images.get(id);
        Bitmap b = BitmapFactory.decodeResource(res, id);
        images.put(id, b);
        return b;
    }

}
