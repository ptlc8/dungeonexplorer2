package com.ptlc.dungeonexplorer2.render;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import com.ptlc.dungeonexplorer2.Game;
import com.ptlc.dungeonexplorer2.R;

import java.util.Arrays;
import java.util.List;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder holder;
    //private DrawingThread drawThread;
    private Game game;

    public GameView(Context context) {
        super(context);
        init();
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        game = new Game() {
            protected void onFinish() {
                if (!((Activity)getContext()).isFinishing())
                    ((Activity)getContext()).finish();
            }
        };
        holder = getHolder();
        holder.addCallback(this);
        /*drawThread = new DrawingThread();
        drawThread.start();*/
        /*setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (event.getY() >= getHeight()/4 && event.getY() < 3*getHeight()/4) {
                if (event.getX() < getWidth()/2)
                    game.leftCard();
                else
                    game.rightCard();

                Canvas cvs = holder.lockCanvas();
                draw(cvs);
                if (cvs != null) holder.unlockCanvasAndPost(cvs);
            }
        }
        return false;
    }

    @Override
    public void draw(Canvas cvs) {
        super.draw(cvs);
        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.rgb(255, 255, 255));
        cvs.drawLine(0, cvs.getHeight()/4, cvs.getWidth(), cvs.getHeight()/4, p);
        cvs.drawLine(cvs.getWidth()/2, cvs.getHeight()/4, cvs.getWidth()/2, 3*cvs.getHeight()/4, p);
        cvs.drawLine(0, 3*cvs.getHeight()/4, cvs.getWidth(), 3*cvs.getHeight()/4, p);
        cvs.drawLine(0, 5*cvs.getHeight()/6, cvs.getWidth(), 5*cvs.getHeight()/6, p);
        cvs.drawLine(cvs.getWidth()/2, 5*cvs.getHeight()/6, cvs.getWidth()/2, cvs.getHeight(), p);
        p.setTextAlign(Paint.Align.CENTER);
        p.setTextSize(cvs.getHeight()/32); // originally : 32
        cvs.drawText(game.getTitle(), cvs.getWidth()/2, cvs.getHeight()/16, p);
        p.setTextSize(cvs.getHeight()/48);
        String[] descWords = game.getDescription().split(" ");
        for (int i = 0; i < 3; i++)
            cvs.drawText(TextUtils.join(" ", Arrays.copyOfRange(descWords, i*descWords.length/3, (i+1)*descWords.length/3)), cvs.getWidth()/2, (3+i)*cvs.getHeight()/24, p);
        drawCard(cvs, game.getLeftCard(), new Rect(0, cvs.getHeight()/4, cvs.getWidth()/2, 3*cvs.getHeight()/4), p); // left
        drawCard(cvs, game.getRightCard(), new Rect(cvs.getWidth()/2, cvs.getHeight()/4, cvs.getWidth(), 3*cvs.getHeight()/4), p); // right
        p.setColor(Color.rgb(255, 255, 255));
        cvs.drawText(game.getPlayer().getHealth()+" ❤", cvs.getWidth()/12, 77*cvs.getHeight()/96, p);
        cvs.drawText("+ "+game.getPlayer().getProtection()+" \uD83D\uDEE1", 3*cvs.getWidth()/12, 77*cvs.getHeight()/96, p);
        cvs.drawText(game.getPlayer().getAgility()+" \uD83C\uDFC3", 5*cvs.getWidth()/12, 77*cvs.getHeight()/96, p);
        cvs.drawText(game.getPlayer().getStrength()+" \uD83C\uDFCB", 7*cvs.getWidth()/12, 77*cvs.getHeight()/96, p);
        cvs.drawText("+ "+game.getPlayer().getDamage()+" \uD83D\uDCA5", 9*cvs.getWidth()/12, 77*cvs.getHeight()/96, p);
        cvs.drawText(game.getPlayer().getCharism()+" \uD83D\uDD76", 11*cvs.getWidth()/12, 77*cvs.getHeight()/96, p);
        Bitmap inv = ImageLoader.loadBitmap(getResources(), R.drawable.inventory);
        cvs.drawBitmap(inv, new Rect(0, 0, inv.getWidth(), inv.getHeight()),
                new Rect(0, 5 * cvs.getHeight() / 6, cvs.getWidth() / 2, cvs.getHeight()), p);
        Bitmap weapon = ImageLoader.loadBitmap(getResources(), R.drawable.front_button);
        cvs.drawBitmap(weapon, new Rect(0, 0, weapon.getWidth(), weapon.getHeight()),
                new Rect(cvs.getWidth() / 2, 5 * cvs.getHeight() / 6, 2 * cvs.getWidth() / 3, cvs.getHeight()), p);
        Bitmap armor = ImageLoader.loadBitmap(getResources(), R.drawable.front_button);
        cvs.drawBitmap(armor, new Rect(0, 0, armor.getWidth(), armor.getHeight()),
                new Rect(2 * cvs.getWidth() / 3, 5 * cvs.getHeight() / 6, 5 * cvs.getWidth() / 6, cvs.getHeight()), p);
        Bitmap tool = ImageLoader.loadBitmap(getResources(), R.drawable.front_button);
        cvs.drawBitmap(tool, new Rect(0, 0, tool.getWidth(), tool.getHeight()),
                new Rect(5 * cvs.getWidth() / 6, 5 * cvs.getHeight() / 6, cvs.getWidth(), cvs.getHeight()), p);
    }

    private void drawCard(Canvas cvs, Card card, Rect dest, Paint p) {
        /*p.setColor(card.getType().getColor());
        cvs.drawRect(dest, p);
        Bitmap edges = BitmapFactory.decodeResource(getResources(), R.drawable.card_edges);
        cvs.drawBitmap(edges, new Rect(0, 0, edges.getWidth(), edges.getHeight()), dest, p);
        edges.recycle();*/
        Bitmap content = card.getImage(getResources());
        cvs.drawBitmap(content, new Rect(0, 0, content.getWidth(), content.getHeight()), dest, p);
    }

    // ----------------------------

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        //drawThread.resumeDrawing();

        Canvas cvs = holder.lockCanvas();
        draw(cvs);
        if (cvs != null) holder.unlockCanvasAndPost(cvs);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int pixelFormat, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        //drawThread.pauseDrawing();
    }

    public void debug(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    /*public class DrawingThread extends Thread {

        private boolean isDrawing = false;

        @Override
        public void run() {
            isDrawing = false;
            while (true) {
                if (!isDrawing) continue;
                Canvas cvs = null;
                try {
                    cvs = holder.lockCanvas();
                    synchronized (holder) {
                        draw(cvs);
                    }
                } finally {
                    if (cvs != null) holder.unlockCanvasAndPost(cvs);
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {}
            }
        }

        public void pauseDrawing() {
            isDrawing = false;
            /*try {
                join();
            } catch (InterruptedException e) {}*//*
        }

        public void resumeDrawing() {
            isDrawing = true;
        }

    }*/

}
