package com.ptlc.dungeonexplorer2.render;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.ptlc.dungeonexplorer2.R;

import java.util.HashMap;
import java.util.Map;

public class Card {

    public static final Card
            Attack = new Card(CardType.ATTACK, "Attaquer", R.drawable.attack_button),
            Continue = new Card(CardType.NEUTRAL, "Continuer", R.drawable.continue_button),
            Death1 = new Card(CardType.NEUTRAL, "Adieu", R.drawable.death1_button),
            Death2 = new Card(CardType.NEUTRAL, "Au revoir monde cruel", R.drawable.death2_button),
            Force = new Card(CardType.STRENGTH, "Forcer", R.drawable.force_button),
            Front = new Card(CardType.LUCK, "En face", R.drawable.front_button),
            Ignore = new Card(CardType.NEUTRAL, "Ignorer", R.drawable.ignore_button),
            Inspect = new Card(CardType.LUCK, "Inspecter", R.drawable.inspect_button),
            Left = new Card(CardType.LUCK, "Gauche", R.drawable.left_button),
            No = new Card(CardType.NEUTRAL, "Non", R.drawable.no_button),
            Open = new Card(CardType.LUCK, "Ouvrir", R.drawable.open_button),
            Ransack = new Card(CardType.LUCK, "Fouiller", R.drawable.ransack_button),
            Right = new Card(CardType.LUCK, "Droite", R.drawable.right_button),
            RunAway = new Card(CardType.AGILITY, "Fuir", R.drawable.runaway_button),
            Trade = new Card(CardType.TRADE, "Commercer", R.drawable.trade_button),
            Try = new Card(CardType.LUCK, "Essayer", R.drawable.try_button),
            Yes = new Card(CardType.NEUTRAL, "Oui", R.drawable.yes_button);

    public enum CardType {

        ATTACK(Color.rgb(219,79,100), "Attaque"), // rouge
        LUCK(Color.rgb(94,192,94), "Chance"), // vert
        AGILITY(Color.rgb(224,224,66), "Agilité"), // jaune
        STRENGTH(Color.rgb(207,153,71), "Force"), // orange
        TRADE(Color.rgb(9,163,181), "Commerce"), // bleu
        DEFENSE(Color.rgb(126,121,201), "Défense"), // violet
        HEALTH(Color.rgb(0,0,0), "Santé"), // ???
        NEUTRAL(Color.rgb(147,147,147), "Neutre"); // gris

        private int color;
        private String name;

        CardType(int color, String name) {
                this.color = color;
                this.name = name;
        }

        public int getColor() {
            return color;
        }

        public String getName() {
            return name;
        }
    }

    private static Map<Integer, Bitmap> images = new HashMap<>();

    private CardType type;
    private String text;
    private int imageResource;

    Card(CardType type, String text, int imageResource) {
        this.type = type;
        this.text = text;
        this.imageResource = imageResource;
    }

    public CardType getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public Bitmap getImage(Resources resources) {
        if (images.get(imageResource) != null) return images.get(imageResource);
        return BitmapFactory.decodeResource(resources, imageResource);
    }

}
